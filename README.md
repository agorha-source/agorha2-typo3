# agorha2-typo3

## Build du container pour la prod

```
cd docker
COMPOSE_FILE=./docker-compose.yml docker compose build --pull
docker tag agorha2/typo3:latest agorha2/typo3:x.x.x
```

## Vuejs

Un projet Vuejs a été initialisé dans Typo3 pour pouvoir utiliser la searchbar sur page d'accueil
    
Le fichier de configuration pour l'url de l'api , se situe : 

    public/typo3conf/ext/agorha_base/Build/Vuidget/src/config/api-config.js

Modifier la constante:  

    const baseApiUrl = 'http://<HOSTNAME>/agorha2-api/';

Puis lancer le build depuis le dossier racine ci-dessous

    public/typo3conf/ext/agorha_base/Build/Vuidget
    
    npm run build
