<?php
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['dbname'] = getenv('MYSQL_DATABASE');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['host'] = getenv('MYSQL_HOST');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['port'] = getenv('MYSQL_PORT');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['password'] = getenv('MYSQL_PASSWORD');
$GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default']['user'] = getenv('MYSQL_USER');

$GLOBALS['NUXT_URL'] = getenv('NUXT_URL');
$GLOBALS['AGORHA2_API_URL'] = getenv('AGORHA2_API_URL');
$GLOBALS['COOKIE_DOMAIN'] = getenv('COOKIE_DOMAIN');
$GLOBALS['RECHERCHE_URL'] = getenv('RECHERCHE_URL');

// Utile pour la connexion utilisateur sur l'indexation des news
$GLOBALS['USER_ADMIN_MAIL'] = getenv('USER_ADMIN_MAIL');
$GLOBALS['USER_ADMIN_PASSWORD'] = getenv('USER_ADMIN_PASSWORD');

// Matomo Tracking
$GLOBALS['MATOMO_URL'] = getenv('MATOMO_URL');
$GLOBALS['MATOMO_ID'] = getenv('MATOMO_ID');

// Configuration pour fonctionner derrière un reverse proxy en https
$GLOBALS['TYPO3_CONF_VARS']['SYS']['reverseProxyIP'] = '*';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['reverseProxyHeaderMultiValue'] = 'last';
if (getenv('TYPO3_REVERSE_PROXY_SSL')) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['reverseProxySSL'] = getenv('TYPO3_REVERSE_PROXY_SSL');
}
$GLOBALS['TYPO3_CONF_VARS']['SYS']['trustedHostsPattern'] = getenv('TYPO3_TRUSTED_HOSTS_PATTERN');
