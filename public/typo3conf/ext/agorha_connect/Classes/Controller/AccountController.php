<?php
namespace Sword\AgorhaConnect\Controller;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Utility\FlashUtility;
use Sword\AgorhaBase\Controller\UtilityController;

/**
 * AccountController
 */
class AccountController extends UtilityController
{
    protected $userCountIsValidated = false;
    protected $mailAlreadyExist = false;
    protected $deleteAccountTracking = false;
  /**
     * /creation-compte
     * action create
     */
    public function createAction()
    {
        // Récupération des informations du formulaire
        if (isset($_POST['tx_agorhaconnect_createaccount'])) {
            $data = $this->getFormData('tx_agorhaconnect_createaccount');
            if(!$this->newPasswordIsStrong($data['userPassword']) ){
                $this->addFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:form_bad_newpassword'), "", 1);
            } else {
              // $this->mailAlreadyExist = true si le mail est déjà dans la base
              $this->saveNewUser($data);
            }
           $this->assignUserData($data['userName'], $data['userFirstName'], $data['userMail']);
        }
        // Numéros aléatoires pour le CAPTCHA
        $number1 = rand(0,9);
        $number2 = rand(0,9);
        $this->view->assignMultiple([
            'number1'=> $number1,
            'number2' => $number2,
            'mailAreadyExist' => $this->mailAlreadyExist
        ]);
    }

    /**
     * /validation-compte
     * action validate
     */
    public function validateAction()
    {
        if(isset($_GET['userId']) && isset($_GET['token'])) {
            $url = $this->getApiUrl() . "/users/validate/" . $_GET['userId'] . "?token=" . $_GET['token'];
            try {
                $response = $this->client->put($url);
                if(isset($response)) {
                    $this->userCountIsValidated = true;
                }
            } catch (RequestException $e) {
                $this->addFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:validateaccount_invalidlink'), "", 2);
            }
        } else {
            // Redirection page accueil si on arrive sur la page au hasard
            $this->redirectToPage($this->uidPage['homepage'], 303);
        }

        $this->view->assign('userCountIsValidated', $this->userCountIsValidated);
    }

    /**
     * /mon-compte
     * action modif
     * @throws GuzzleException
     */
    public function modifAction()
    {
        //Est-que l'utilisateur est connecté ?
        if ($this->isAuthenticated()) {
            // Récupération des informations actuelles de l'utilisateur
            $userData = $this->getCurrentUser();
            if($userData && !isset($_POST['tx_agorhaconnect_modifaccount'])) {
                $this->assignUserData($userData->name, $userData->firstName, $userData->mail);
            }

            // Récupération des informations du formulaire
            if (isset($_POST['tx_agorhaconnect_modifaccount'])) {
               $data = $this->getFormData("tx_agorhaconnect_modifaccount");
               if(!empty($data['userNewPassword']) && !$this->newPasswordIsStrong($data['userNewPassword'])) {
                   $this->addFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:form_bad_newpassword'), "", 1);
               } else {
                    $this->modifUserData($data);
               }

               $this->assignUserData($data['userName'], $data['userFirstName'], $data['userMail']);
            }

            $this->view->assignMultiple([
                'agorhaApiUrl' => $this->getApiUrl(),
                'userRole' =>  $this->getUserRole(),
            ]);
        } else {
            // Si user non connecté, redirection page Login
            $this->redirectToPage($this->uidPage['login'], 302);
        }
    }

    /**
     * Suppression de compte
     *
     * @throws GuzzleException
     */
    public function deleteAction() {
        if ($_POST['tx_agorhaconnect_modifaccount']) {
            $url = $this->getApiUrl() . "/users";
            try {
                $response = $this->client->request('DELETE', $url, ['cookies' => $this->getCookieJar()]);
                if ($response) {
                    $flashController = new FlashUtility();
                    $flashController->createFlashMessageWithQueueIdentifier("L'utilisateur a bien été supprimé", "Utilisateur supprimé", 0, 'loginQueue');

                    // Tracking matomo suppression compte si consentement
                    if ($this->matomoTracker !== null) {
                        $this->matomoTracker->doTrackEvent('account', 'deleteAccount');
                    }

                    $this->setSessionToNull();
                    $this->deleteCookie();
                    $this->redirectToPage($this->uidPage['login'], 303);
                }
            } catch (RequestException $e) {
                $this->addFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:deleteaccount_error'), 'Erreur', 2);
                $this->redirectToPage($this->uidPage['account'], 303);
            }
        }
    }

  /**
   * Retourne les infos créées dans le formulaire
   *
   * @param $table
   * @return array
   */
    private function getFormData($table)
    {
       return [
           'userName' =>  $_POST[$table]['userName'],
           'userFirstName' => $_POST[$table]['userFirstName'],
           'userlang' => $_POST[$table]['userlang'],
           'userMail' => $_POST[$table]['userMail'],
           'userPassword' => $_POST[$table]['userPassword'],
           'userOldPassword' => $_POST[$table]['userOldPassword'],
           'userNewPassword' => $_POST[$table]['userNewPassword'],
           'captcha' => $_POST[$table]['captcha']
       ];
    }

    /**
     * Sauvegarde le nouvel utilisateur
     * @param $user
     */
    private function saveNewUser($user)
    {
      $additionalOptions = [
            'form_params' => [
                'name' => $user['userName'],
                'firstName' => $user['userFirstName'],
                'mail' => $user['userMail'],
                'password' => $user['userPassword']
            ]
        ];
        $url = $this->getUserApiUrl();
        try {
            $response = $this->client->post($url, $additionalOptions);
            $response = $response->getBody()->read($response->getBody()->getSize());
            if (isset($response)) {
               // tracking matomo création compte si consentement et redirection homepage
                if ($this->matomoTracker !== null) {
                    $this->matomoTracker->doTrackEvent('account', 'createAccount');
                }
               $this->redirectToHomepageWithFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:createaccount_usercountiscreated'), '', 303);
            }
        } catch (RequestException $e) {
            $this->mailAlreadyExist = true;
        }
    }

    /**
     *
     * @param $user
     * @throws GuzzleException
     */
    private function modifUserData($user)
    {
        $additionalOptions = [
            'cookies' => $this->getCookieJar(),
            'form_params' => [
                'name' => $user['userName'],
                'firstName' => $user['userFirstName'],
                'mail' => $user['userMail'],
                'oldPassword' => $user['userOldPassword'],
                'newPassword' => $user['userNewPassword']
            ]
        ];
        $url = $this->getUserApiUrl();
        try {
            // On envoie les modifications
           $data = $this->client->request('PUT', $url, $additionalOptions);
           // L'api nous renvoie les nouvelles date de l'utilisateur
           $userData = \GuzzleHttp\json_decode($data->getBody());
           // On met ces infos en session
           if ($userData) {
               $this->setUserData($userData);
           }
            $this->redirectToHomepageWithFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:modifaccount_datasaved'), '', 303);
        } catch (RequestException $e) {
          $message = $this->getMessageFromResponse($e->getMessage());
          $this->addFlashMessage($message, "", 2);
        }
    }

    private function setUserData($userData){
        $userName = $userData->name;
        $userFirstName = $userData->firstName;
        $user = ucfirst($userFirstName) . ' ' . ucfirst($userName);
        $GLOBALS['TSFE']->fe_user->setKey('ses','user',$user);
    }

    private function getUserApiUrl() {
        return $this->getApiUrl() . "/users";
    }

    /**
     * Récupère les données de l'utilsateur connecté
     *
     * @return bool|mixed
     */
    private function getCurrentUser()
    {
        $userData = null;
        $url = $this->getApiUrl() . '/users/current';
        try{
           $response = $this->client->request('GET', $url, ['cookies' => $this->getCookieJar()]);
           $userData = \GuzzleHttp\json_decode($response->getBody());
        } catch (GuzzleException $e) {}
        return $userData ? $userData : false;
    }

    private function assignUserData($name, $firstname, $mail)
    {
        $this->view->assignMultiple([
            'userName' => $name,
            'userFirstName' => $firstname,
            'userMail' => $mail
        ]);
    }
}
