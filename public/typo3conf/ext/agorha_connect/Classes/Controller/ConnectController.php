<?php
namespace Sword\AgorhaConnect\Controller;

use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;

/**
 * ConnectController
 */
class ConnectController extends UtilityController
{

    /**
     * /login
     * action login
     */
    public function loginAction()
    {
        //Est-que l'utilisateur est connecté ?
        if(!$this->isAuthenticated()) {
            // Validation du formulaire de connexion
            if (isset($_POST['tx_agorhaconnect_connect'])) {
              $url = $this->getAuthenticateUrl();
              try {
                  $response = $this->client->get($url);
              } catch (RequestException $e) {
                  // Matomo tracking echec connection si consentement
                  if ($this->matomoTracker !== null) {
                      $this->matomoTracker->doTrackEvent('account', 'connection', 'failure');
                  }
                $this->addFlashMessage('Les informations de connexion que vous avez renseignées ne sont pas correctes.', '', 3);
              }
              if (isset($response) && ($response->getStatusCode() === 200)) {
                  // Matomo tracking connection réussie si consentement
                  if ($this->matomoTracker !== null) {
                      $this->matomoTracker->doTrackEvent('account', 'connection', 'success');
                  }
                $data = json_decode($response->getBody());
                // Met en session Typo3 les infos utilisateurs
                $this->setUserSession($data);
                // Récupère le cookie et met en session
                $apiCookie = $response->getHeaderLine('Set-Cookie');
                $this->setCookie($apiCookie);
                // Redirection vers page d'accueil
                $this->redirectToPage($this->uidPage['homepage'], 200);
              }
            }
        } else {
            // Si user connecté, redirection page Accueil
            $this->redirectToPage($this->uidPage['homepage'], 200);
        }
    }

    /**
     * /logout
     * action logout
     */
    public function logoutAction(){
        try {
            $url = $GLOBALS['AGORHA2_API_URL'] . '/users/logout';
            $this->client->post( $url);
        } catch (RequestException $e){}
        $this->setSessionToNull();
        $this->deleteCookie();
        $this->redirectToPage($this->uidPage['homepage'], 302);
    }

    /**
     * URL d'authentification utilisateur
     * @return string
     */
    private function getAuthenticateUrl(){
        $apiUrl = $this->getApiUrl() . '/users/authenticate';
        $userId = $_POST['tx_agorhaconnect_connect']['userId'];
        $userPassword = $_POST['tx_agorhaconnect_connect']['userPassword'] ? urlencode($_POST['tx_agorhaconnect_connect']['userPassword']) : null;
        return $url = $apiUrl . '?login=' . $userId . '&password=' . $userPassword;
    }

    /**
     * Met en session Typo3 les informations utilisateur
     * @param $data
     */
    private function setUserSession($data){
        $userName = $data->name;
        $userFirstName = $data->firstName;
        $user = ucfirst($userFirstName) . ' ' . ucfirst($userName);
        $role = $data->authorizations->role;
        $GLOBALS['TSFE']->fe_user->checkPid = '';
        $GLOBALS['TSFE']->fe_user->setKey('ses','user',$user);
        $GLOBALS['TSFE']->fe_user->setKey('ses','role',$role);
    }

    /**
     * Permet de mettre en session le cookie reçu
     * lors de l'authentification de l'utilisateur
     * @param $cookie
     */
    private function setCookie($cookie){
        $data = explode(';', $cookie);
        $userCookie = explode('=', $data[0]);
        $value = $userCookie[1];
        /*
        setCookie($name, $value = "", $expire = 0, $path = "", $domain = "", $secure = false, $httponly = false)
        HttpOnly permet de bloquer l'accés des cookies via l'API JavaScript document.cookie
        */
        setcookie('JSESSIONID',$value,0,'','',false,true);
    }
}
