<?php
namespace Sword\AgorhaConnect\Controller;

use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;

/**
 * PasswordController
 */
class PasswordController extends UtilityController
{

    /**
     * /mot-de-passe-oublie
     * action forgotten
     */
    public function forgottenAction()
    {
        if(isset($_POST['tx_agorhaconnect_forgottenpassword'])) {
            $mail = $_POST['tx_agorhaconnect_forgottenpassword']['userMail'];
            $url = $this->getApiUrl() . "/users/resetPassword?mail=" . $mail;
            try {
               $this->client->get($url);
               /* TODO redirection à corriger car elle crée un double appel */
               // $this->redirectToHomepageWithFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:forgottenpassword_reinitialisation_mail_is_send'), '');
            } catch (RequestException $e) {}
            $this->addFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:forgottenpassword_reinitialisation_mail_is_send'), "", 0);
        }
    }

    /**
     * /reinitialisation-mot-de-passe
     * action reinitialize
     */
    public function reinitializeAction()
    {
        if(isset($_GET['userId']) && isset($_GET['token'])) {
            $url = $this->getApiUrl() . "/users/resetPassword/" . $_GET['userId'] ."/isValid?token=" . $_GET['token'];
            try {
                // Validité du token
                $this->client->get($url);
            } catch (RequestException $e) {
                // Si le token est expiré, on redirige vers la page d'accueil
                $this->redirectToPage($this->uidPage['homepage'], 302);
            }
            $this->view->assignMultiple([
                'userId' => $_GET['userId'],
                'token' => $_GET['token']
            ]);
        } else {
            // Si l'utilisateur valide le formulaire
            if (isset($_POST['tx_agorhaconnect_reinitializepassword']) && isset($_POST['tx_agorhaconnect_reinitializepassword']['userPassword']) ) {
                $password = $_POST['tx_agorhaconnect_reinitializepassword']['userPassword'];
                $userId = $_POST['userId'];
                $token = $_POST['token'];
                if (!$this->newPasswordIsStrong($password)) {
                    $this->addFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:form_bad_newpassword'), "", 1);
                } else {
                    $url = $this->getApiUrl()."/users/updatePassword/".$userId."?newPassword=".$password."&token=".$token;
                    try {
                        $this->client->put($url);
                        $flashController = new \Sword\AgorhaBase\Utility\FlashUtility();
                        $flashController->createFlashMessageWithQueueIdentifier($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:reinitialisepassword_password_modified'), "",0, 'loginQueue');
                        $this->redirectToPage($this->uidPage['login'], 303);
                    } catch (RequestException $e) {
                        $this->addFlashMessage($GLOBALS['TSFE']->sL('LLL:EXT:agorha_connect/Resources/Private/Language/locallang.xlf:reinitialisepassword_password_notmodified'), "", 2);
                    }
                }
                $this->view->assignMultiple([
                    'userId' => $userId,
                    'token' => $token
                ]);
            } else{
                // Si on arrive sur la page par erreur
                $this->redirectToPage($this->uidPage['homepage'], 303);
            }
        }
    }

}
