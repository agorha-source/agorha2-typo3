<?php
defined('TYPO3_MODE') or die;

$boot = function() {
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

    // Connect
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaConnect',
        'Connect',
        [
            'Connect' => 'login'
        ],
        [
            'Connect' => 'login'
        ]
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaConnect',
        'Logout',
        [
            'Connect' => 'logout'
        ],
        [
            'Connect' => 'logout'
        ]
    );

    // Account
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaConnect',
        'ModifAccount',
        [
            'Account' => 'modif, delete'
        ],
        [
            'Account' => 'modif, delete'
        ]
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaConnect',
        'CreateAccount',
        [
            'Account' => 'create'
        ],
        [
            'Account' => 'create'
        ]
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaConnect',
        'ValidateAccount',
        [
            'Account' => 'validate'
        ],
        [
            'Account' => 'validate'
        ]
    );
    // Password
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaConnect',
        'ForgottenPassword',
        [
            'Password' => 'forgotten'
        ],
        [
            'Password' => 'forgotten'
        ]
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaConnect',
        'ReinitializePassword',
        [
            'Password' => 'reinitialize'
        ],
        [
            'Password' => 'reinitialize'
        ]
    );

};

$boot();
unset($boot);
