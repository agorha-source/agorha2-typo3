<?php

// Connect
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaConnect',
    'Connect',
    'Connexion',
    'EXT:agorha_base/Resources/Public/Icons/account_circle.svg'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaConnect',
    'Logout',
    'Déconnexion',
    'EXT:agorha_base/Resources/Public/Icons/power.svg'
);

// Account
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaConnect',
    'ModifAccount',
    'Modification Compte'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaConnect',
    'CreateAccount',
    'Création de Compte',
    'EXT:agorha_base/Resources/Public/Icons/account_circle.svg'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaConnect',
    'ValidateAccount',
    'Validation de Compte',
    'EXT:agorha_base/Resources/Public/Icons/account_circle.svg'
);

// Password
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaConnect',
    'ForgottenPassword',
    'Mot de passe oublié',
    'EXT:agorha_base/Resources/Public/Icons/password.svg'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaConnect',
    'ReinitializePassword',
    'Réinitialisation mot de passe',
    'EXT:agorha_base/Resources/Public/Icons/password.svg'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaconnect_login'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaconnect_logout'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaconnect_modifaccount'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaconnect_createaccount'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaconnect_forgottenpassword'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaconnect_reinitialisepassword'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaconnect_validatepassword'] = 'recursive,select_key,pages';
