let mailIsChanged = false;
function mailChanged(oldMail) {
  let mail = document.getElementById("mail");
  let passwordOne = document.getElementById("password1");
  if(mail.value !== oldMail) {
    passwordOne.required = true;
    mailIsChanged = true;
  } else {
    passwordOne.required = false;
  }
}

function comparePasswords() {
    let passwordOne = document.getElementById("password1");
    let passwordTwo = document.getElementById("password2");
    let button = document.getElementById("submitModifForm");
    if (passwordOne.value && !mailIsChanged){
      passwordTwo.required = true;
    } else if(passwordTwo.value && !mailIsChanged) {
      passwordOne.required = true;
    } else {
      passwordOne.required = false;
      passwordTwo.required = false;
    }
    if (passwordOne.value && passwordTwo.value) {
      button.disabled = passwordOne.value === passwordTwo.value;
    }
}

function showPassword(number) {
    let x = document.getElementById("password"+number);
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

function correctCaptcha(nb1, nb2) {
    let captcha = document.getElementById("captcha");
    let button = document.getElementById("submitCreateForm");

  if (parseInt(captcha.value) !== (nb1 + nb2)) {
        captcha.style.backgroundColor = 'red';
        button.disabled = true;
    } else {
        captcha.style.backgroundColor = 'white';
        button.disabled = false;
    }
}

function deleteUser(apiUrl) {
    let url = apiUrl + "/users";
    $.ajax({
        url: url ,
        method: "DELETE",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            window.location.replace("/");
        },
        error: function () {
            let modal = $('.modal');
            modal.modal('hide');
            let toast = $('.toast');
            toast.toast({
                delay: 5000,
                autohide: true
            });
            toast.toast('show');
        }
    })
}
