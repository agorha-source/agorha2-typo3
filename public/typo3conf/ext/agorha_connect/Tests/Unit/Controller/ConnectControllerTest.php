<?php
namespace Sword\AgorhaConnect\Tests\Unit\Controller;

/**
 * Test case.
 */
class ConnectControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Sword\AgorhaConnect\Controller\ConnectController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Sword\AgorhaConnect\Controller\ConnectController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
