<?php
namespace Sword\AgorhaConnect\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ConnectTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Sword\AgorhaConnect\Domain\Model\Connect
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Sword\AgorhaConnect\Domain\Model\Connect();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSurnameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSurname()
        );
    }

    /**
     * @test
     */
    public function setSurnameForStringSetsSurname()
    {
        $this->subject->setSurname('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'surname',
            $this->subject
        );
    }
}
