<?php
defined('TYPO3_MODE') or die;

$boot = function() {

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaNuxtjs',
        'Search',
        [
            'Search' => 'search, displayNotice'
        ],
        [
            'Search' => 'search, displayNotice'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaNuxtjs',
        'Media',
        [
            'Media' => 'index'
        ],
        [
            'Media' => 'index'
        ]
    );

  unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['TYPO3\CMS\Frontend\Page\PageGenerator']['generateMetaTags']['canonical']);
};

$boot();
unset($boot);



