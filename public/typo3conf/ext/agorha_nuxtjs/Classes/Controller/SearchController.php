<?php

namespace Sword\AgorhaNuxtjs\Controller;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * SearchController
 */
class SearchController extends UtilityController
{

    public function __construct()
    {
        parent::__construct();
        $this->deleteMetaTag();
    }

    /**
     *  /recherche
     * action search
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchAction() {
        $cookie = $this->isUserConnected();
        try {
            $url = $this->getUrl();
            $response = $this->guzzleRequestToString($url, $cookie);
            $data = $this->getDataFromResponse($response);
            if($data) {
                $this->view->assign('nuxt_data', $data);
            } else {
                $this->redirectToPage($this->uidPage['500'],302);
            }
        }
        catch (RequestException $e){
            $this->redirectToPage($this->uidPage['404'],302);
        }
    }

    /**
     *  /ark:/uidNotice
     * action displayNotice
     */
    public function displayNoticeAction() {
        $cookie = $this->isUserConnected();
        try {
            $url = $this->getUrl('ark:');
            $response = $this->guzzleRequestToString($url, $cookie);
            $data = $this->getDataFromResponse($response);
            if($data) {
                $this->view->assign('nuxt_data', $data);
            } else {
                $this->redirectToPage($this->uidPage['500'],302);
            }
        } catch (RequestException $e){
            $this->redirectToPage($this->uidPage['404'],302);
        }
    }

    /**
     * Fait appel à isAuthenticated() pour vérifier la connexion de l'utilisateur
     * Récupère le cookie en session si user connecté
     * @return CookieJar
     */
    private function isUserConnected() {
        if ($this->isAuthenticated()) {
            $this->cookie = $this->getCookieJar();
        }
        return $this->cookie;
    }

    /**
     * Fait appel à la fonction splitTitleAndBodyFromPage() pour nettoyer $reponse
     * Return $data si $response non vide ou false
     * @param $response
     * @return bool|string
     */
    private function getDataFromResponse($response) {
        if(isset($response)) {
            // Récupération du body et des balises head présents dans la réponse
            $data = $this->splitTitleAndBodyFromPage($response);
        } else {
            $data = false;
        }
        return $data;
    }
    /**
     * Permet de retourner l'url de requête
     * Si param $uuid donné : requête description notice
     * Sinon : Affiche la page principale de Nuxtjs
     * @param null $uuid
     * @return string
     */
    private function getUrl($ark=null){
        $url = $this->getNuxtUrl();
        if(!$ark==null) {
            $database = GeneralUtility::_GP('database') ? '?database=' . GeneralUtility::_GP('database') : '';
            $searchURI = $url . $ark.'/54721/' . GeneralUtility::_GP('uuid') . $database;
        } else {
          $page = GeneralUtility::_GP('page') ? GeneralUtility::_GP('page') : 1;
          $pageSize = GeneralUtility::_GP('pageSize') ? GeneralUtility::_GP('pageSize') : 20;
          $terms = GeneralUtility::_GP('terms') ? '&terms=' . urlencode(GeneralUtility::_GP('terms')) : '';
          $fieldSort = GeneralUtility::_GP('fieldSort') ? '&fieldSort='.GeneralUtility::_GP('fieldSort') : '';
          $type = GeneralUtility::_GP('type') ? '&type='.GeneralUtility::_GP('type') : '';
          $noticeType = GeneralUtility::_GP('noticeType') ? '&noticeType=' .GeneralUtility::_GP('noticeType') : '';
          $filters = GeneralUtility::_GP('filters') ? '&filters=' .GeneralUtility::_GP('filters') : '';
          $selectionId = GeneralUtility::_GP('selectionId') ? '&selectionId=' .GeneralUtility::_GP('selectionId') : '';
          $facets = GeneralUtility::_GP('facets') ? '&facets=' .GeneralUtility::_GP('facets') : '';

          $searchURI = $url . 'recherche?page=' . $page . '&pageSize=' . $pageSize . $terms . $fieldSort .  $type .  $noticeType .  $filters . $selectionId .  $facets;
        }
        return $searchURI;
    }

}
