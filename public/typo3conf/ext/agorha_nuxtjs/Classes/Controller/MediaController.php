<?php

namespace Sword\AgorhaNuxtjs\Controller;

use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * MediaController
 */
class MediaController extends UtilityController
{

    public function __construct()
    {
        parent::__construct();
        $this->deleteMetaTag();
    }

    /**
     *  /recherche-media
     * action index
     */
    public function indexAction() {
        // utilisateur connecté ?
        if($this->isAuthenticated() && $this->getUserRole() === 'ADMIN' || $this->getUserRole() === 'CONTRIB' || $this->getUserRole() === 'DATABASE_MANAGER') {
            try {
                $url = $this->getUrl();
                $response = $this->guzzleRequestToString($url, $this->isUserConnected());
                $data = $this->getDataFromResponse($response);
                if($data) {
                    $this->view->assign('nuxt_data', $data);
                } else {
                    $this->redirectToPage($this->uidPage['500'],302);
                }
            }
            catch (RequestException $e){
                $this->redirectToPage($this->uidPage['404'],302);
            }
        } else {
            // Si ce n'est pas le cas : redirection vers la page de Login (uid: 28)
            $this->redirectToPage($this->uidPage['login'], 302);
        }
    }

    /**
     * Fait appel à isAuthenticated() pour vérifier la connexion de l'utilisateur
     * Récupère le cookie en session si user connecté
     * @return CookieJar
     */
    private function isUserConnected() {
        if ($this->isAuthenticated()) {
            $this->cookie = $this->getCookieJar();
        }
        return $this->cookie;
    }

    /**
     * Fait appel à la fonction splitTitleAndBodyFromPage() pour nettoyer $reponse
     * Return $data si $response non vide ou false
     * @param $response
     * @return bool|string
     */
    private function getDataFromResponse($response) {
        if(isset($response)) {
            // Récupération du body et des balises head présents dans la réponse
            $data = $this->splitTitleAndBodyFromPage($response);
        } else {
            $data = false;
        }
        return $data;
    }

    /**
     * Permet de retourner l'url de requête
     * @return string
     */
    private function getUrl() {
      $url = $this->getNuxtUrl();
      $terms = GeneralUtility::_GP('terms') ? '&terms=' . urlencode(GeneralUtility::_GP('terms')) : '';
      $page = GeneralUtility::_GP('page') ? GeneralUtility::_GP('page') : 1;
      $pageSize = GeneralUtility::_GP('pageSize') ? GeneralUtility::_GP('pageSize') : 20;
      $fieldSort = GeneralUtility::_GP('fieldSort') ? '&fieldSort='.GeneralUtility::_GP('fieldSort') : '';
      $facets = GeneralUtility::_GP('facets') ? '&facets=' .GeneralUtility::_GP('facets') : '';
      return $url . 'media?page=' . $page . '&pageSize=' . $pageSize . $terms . $fieldSort . $facets;
    }

}
