<?php
namespace Sword\AgorhaNuxtjs\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Solene <solene.murcia@sword-group.com>
 */
class SearchControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Sword\AgorhaNuxtjs\Controller\SearchController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Sword\AgorhaNuxtjs\Controller\SearchController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    protected function createURLFunctionReturnAnCorrectURL(){

    }

}
