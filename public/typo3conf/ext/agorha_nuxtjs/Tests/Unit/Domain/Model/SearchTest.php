<?php
namespace Sword\AgorhaNuxtjs\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Solene <solene.murcia@sword-group.com>
 */
class SearchTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Sword\AgorhaNuxtjs\Domain\Model\Search
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Sword\AgorhaNuxtjs\Domain\Model\Search();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
