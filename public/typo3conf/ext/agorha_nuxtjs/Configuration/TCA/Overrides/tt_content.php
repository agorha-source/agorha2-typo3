<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaNuxtjs',
    'Search',
    'Recherche',
    'EXT:agorha_base/Resources/Public/Icons/loupe.svg'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaNuxtjs',
    'Media',
    'Recherche Média',
    'EXT:agorha_base/Resources/Public/Icons/export.svg'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhanuxtjs_search'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhanuxtjs_media'] = 'recursive,select_key,pages';

