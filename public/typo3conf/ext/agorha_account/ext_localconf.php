<?php
defined('TYPO3_MODE') or die;

$boot = function() {

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaAccount',
        'ListUsers',
        [
            'Users' => 'list'
        ],
        [
            'Users' => 'list'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaAccount',
        'DetailUser',
        [
            'Users' => 'detail'
        ],
        [
            'Users' => 'detail'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaAccount',
        'ListResearch',
        [
            'SavedResearches' => 'list'
        ],
        [
            'SavedResearches' => 'list'
        ]
    );

    unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['TYPO3\CMS\Frontend\Page\PageGenerator']['generateMetaTags']['canonical']);
};

$boot();
unset($boot);
