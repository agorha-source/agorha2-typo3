
/**
 * Templates/SavedResearches/List.html
 *
 * Fonction de suppression de la recherche sauvegardée
 *
 * @param searchId - identifiant de la recherche sauvegardée
 * @param apiUrl - url de l'api
 */
function deleteSavedSearch(searchId, apiUrl) {
    $('.modal').modal('show');
    let url = apiUrl + "/users/savedSearches/" + searchId;
    $('#modal_valid_delete').click(function() {
        $.ajax({
            url: url ,
            method: "DELETE",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function () {},
            error: function () {},
            complete: function (){
                window.location.replace("/recherches-enregistrees");
            },
        })
    })
}

