/**
 * Templates\Users\GetUser.html
 *
 * Fonction de réinitialisation du mot de passe de
 * l'utilisateur dont le mail est en paramètre
 *
 * @param mail - mail de l'utilisateur
 * @param apiUrl - url de l'api
 */
function reinitPassword(mail, apiUrl) {
    let finalUrl = apiUrl + "/users/resetPassword?mail=" + mail;
    $.ajax({
        url: finalUrl,
        method: "GET",
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function () {
            let toast = $('.toast');
            toast.toast({
                delay: 5000,
                autohide: true
            });
            toast.toast('show');
        },
        error: function () {}
    });
}

/**
 * Templates\Users\Detail.html
 *
 * Suppression utilisateur dont l'id est en paramètre
 *
 * @param id - identifiant de l'utilisateur
 * @param apiUrl - url de l'api
 */
function deleteUser(id, apiUrl) {
    $('.modal').modal('show');
    let url = apiUrl + "/administration/users/" + id;
    $('#modal_valid_delete').click(function() {
        $.ajax({
            url: url,
            method: "DELETE",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function () {window.location.replace("/gerer-les-utilisateurs")},
            error: function (e) { console.log(e);}
        })
    })
}

/**
 * Templates\Users\Detail.html
 *
 * Active le multiselect select2
 */
$(document).ready(function() {
  if (document.getElementById("userData")) {
    activeSelect2();
    showDatabasesSelect();
  }
  setTimeout(function() {
    $('.user-list-flash-message').slideUp("slow");
  }, 5000);
});

/**
 * Permet d'afficher le block de sélection de base de données
 * seulement si l'utilisateur a les droits de reponsable de bases de données
 */
function showDatabasesSelect() {
    let userRoleSelect = document.getElementById("userRoleSelect");
    let userDatabaseSelect = document.getElementById("userDatabaseSelect");
    if(userRoleSelect.value === 'DATABASE_MANAGER') {
      userDatabaseSelect.style.display = 'block';
    } else {
      userDatabaseSelect.style.display = 'none';
    }
}

function activeSelect2() {
  $('.select-database').select2({
    placeholder: 'Choisissez une base de données',
    closeOnSelect: false,
    maximumSelectionLength: 5
  });
}
