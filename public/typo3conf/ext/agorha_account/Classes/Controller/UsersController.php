<?php

namespace Sword\AgorhaAccount\Controller;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Sword\AgorhaBase\Utility\FlashUtility;
use Sword\AgorhaBase\Controller\UtilityController;

/**
 * Page de gestion des utilisateurs par un administrateur
 */
class UsersController extends UtilityController
{

    public function listAction()
    {
        if ($this->isAuthenticated() && $this->isAdmin()) {
            if (isset($_POST['tx_agorhaaccount_listusers']['userFilter'])) {
                // Requête avec termes recherchés
                $users = $this->getUsers($_POST['tx_agorhaaccount_listusers']['userFilter']);
            } else {
                // Requête simple
                $users = $this->getUsers(null);
            }
            $this->view->assign('users', $users);
        } else {
            $this->redirectToPage($this->uidPage['404'], 303);
        }
    }

    public function detailAction()
    {
        /* L'utilisateur arrive pour la première fois sur la page
           On vérifie qu'il est bien authentifié et a les bons droits
           l'id doit être en paramètre */
        if ($this->isAuthenticated() && $this->isAdmin() && $_GET['userId']) {
            $userData = $this->getUserData($_GET['userId']);
            $databases = $this->getDatabases();
            if($userData->authorizations->database && $databases) {
              $this->setSelectedDatabases($databases, $userData->authorizations->database);
            }
            $this->view->assignMultiple([
                'userStatus' => $userData->internal->status,
                'userCreatedDate' => $userData->internal->createdDate,
                'userName' => $userData->internal->lastname,
                'userFirstName' => $userData->internal->firstname,
                'userMail' => $userData->internal->login,
                'userRole' => $userData->authorizations->role,
                'userId' => $userData->internal->uuid,
                'apiUrl' => $this->getApiUrl(),
                'databases' => $databases,
                'userElasticId' => $userData->id
            ]);
        /* L'utilisateur valide la modification */
        } else if ($this->isAuthenticated() && $this->isAdmin() && isset($_POST['tx_agorhaaccount_detailuser']))
        {
            $databases = $this->getDatabases();
            if($databases && $_POST['userDatabases']) {
              $this->setSelectedDatabases($databases, $_POST['userDatabases']);
            }
            $this->patchUser($_POST['userElasticId'], $_POST['tx_agorhaaccount_detailuser']['userRole'], $_POST['userDatabases']);
            $this->view->assignMultiple([
                'userStatus' => $_POST['userStatus'],
                'userCreatedDate' => $_POST['userCreatedDate'],
                'userName' => $_POST['userName'],
                'userFirstName' => $_POST['userFirstName'],
                'userMail' => $_POST['userMail'],
                'userRole' => $_POST['tx_agorhaaccount_detailuser']['userRole'],
                'userId' => $_POST['userId'],
                'apiUrl' => $this->getApiUrl(),
                'databases' => $databases,
                'userElasticId' => $_POST['userElasticId']
            ]);
        } else {
            $this->redirectToPage($this->uidPage['getUsers'], 303);
        }
    }

    /**
     * Modifie les droits de l'utilisateur
     *
     * @param $userElasticId
     * @param $userRole
     * @param $userDatabases
     * @throws ClientException
     */
    private function patchUser($userElasticId,$userRole,$userDatabases)
    {
      if ($userDatabases) {
        $databases = [];
        for ($i = 0; $i < count($userDatabases); $i++) {
          $database = new \stdClass();
          $database->databaseId = $userDatabases[$i];
          array_push($databases, $database);
        }
      }
      $additionalOptions = [
          'cookies' => $this->getCookieJar(),
          'json' => [
              'role' => $userRole,
              'database' => $databases ? $databases : null
          ]
      ];
      $url = $this->getApiUrl() . "/administration/users/" . $userElasticId . "/authorizations";
      try {
        $this->client->request('PATCH', $url, $additionalOptions);
        $flashController = new FlashUtility();
        $flashController->createFlashMessageWithQueueIdentifier("L'utilisateur a bien été modifié", "Modification réussie", 0, 'listUserQueue');
        $this->redirectToPage($this->uidPage['getUsers'],303);
      } catch (ClientException $e) {
          $message = $this->getMessageFromResponse($e->getMessage());
          $this->addFlashMessage($message, "", 2);
      } catch (GuzzleException $e) {
          $message = $this->getMessageFromResponse($e->getMessage());
          $this->addFlashMessage($message, "", 2);
      }
    }

    /**
     * Permet de sélectionner les bases de données
     * dans le select2 appartenant déjà à l'utilisateur
     *
     * @param $databases
     * @param $userDatabases
     */
    private function setSelectedDatabases($databases, $userDatabases)
    {
        for ($i = 0; $i < count($databases); $i++) {
            for ($j = 0; $j < count($userDatabases); $j++) {
                if ($databases[$i]->internal->uuid === $userDatabases[$j] || $databases[$i]->internal->uuid === $userDatabases[$j]->databaseId){
                    $databases[$i]->selected = 'true';
                }
            }
        }
    }

    /**
     * Récupère la liste des 20 premiers utilisateurs
     *
     * @param null $filter
     * @return bool|mixed
     */
    private function getUsers($filter = null)
    {
        try {
            $url = $this->getUsersApiUrl($filter, 1);
            $response = $this->client->get($url, ['cookies' => $this->getCookieJar()]);
        } catch (ClientException $e) {
        }
        if (isset($response) && ($response->getStatusCode() === 200)) {
            $data = \GuzzleHttp\json_decode($response->getBody());
            return $this->getAllUsers($data, $filter);
        } else {
            return false;
        }
    }

    /**
     * Récupère tous les utilisateurs
     *
     * @param $data
     * @param $filter
     * @return array
     */
    private function getAllUsers($data, $filter)
    {
        $nbrUsers = $data->count;
        $usersArray = $data->results;
        for ($i = 2; $i <= ceil($nbrUsers / 20); $i++) {
            $url = $this->getUsersApiUrl($filter, $i);
            $response = $this->client->get($url, ['cookies' => $this->getCookieJar()]);
            $data = \GuzzleHttp\json_decode($response->getBody());
            $usersArray = array_merge($usersArray, $data->results);
        }
        return $usersArray;
    }

    /**
     * Récupère les informations d'un utilisateur
     * @param $id - Identifiant de l'utilisateur
     * @return bool|mixed
     */
    private function getUserData($id)
    {
        try {
            $url = $this->getApiUrl() . '/administration/users/' . $id;
            $response = $this->client->get($url, ['cookies' => $this->getCookieJar()]);
        } catch (ClientException $e) {
            $this->redirectToPage($this->uidPage['getUsers'], 303);
        }
        if (isset($response) && ($response->getStatusCode() === 200)) {
            return \GuzzleHttp\json_decode($response->getBody());
        } else {
            return false;
        }
    }

    private function getUsersApiUrl($filter, $page)
    {
        return $this->getApiUrl() . '/administration/users/search?terms=' . $filter . '&page=' . $page . '&pageSize=20';
    }

    /**
     * Récupère la liste des bases de données
     *
     * @return mixed|null
     */
    private function getDatabases()
    {
        try {
            $url = $this->getApiUrl() . '/database/list';
            $response = $this->client->get($url, ['cookies' => $this->getCookieJar()]);
        } catch (ClientException $e) {
            return null;
        }
        $data = \GuzzleHttp\json_decode($response->getBody());
        return $data->databases;
    }
}
