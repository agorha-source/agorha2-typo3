<?php

namespace Sword\AgorhaAccount\Controller;

use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * SavedResearchesController
 */
class SavedResearchesController extends UtilityController
{

    /**
     *  /recherches-enregistrees
     * action list
     */
    public function listAction() {
        // Récupération du nombre de sélections souhaité
        $savedSearchesNumberPerPage = 10;
        if (GeneralUtility::_GP('itemsPerPage')) {
            $savedSearchesNumberPerPage = GeneralUtility::_GP('itemsPerPage');
        }

        if ($this->isAuthenticated()) {
            if (isset($_POST['tx_agorhaaccount_listresearch']['noticeFilter'])) {
                // Requête avec termes recherchés
                $data = $this->getUserSavedSearches($_POST['tx_agorhaaccount_listresearch']['noticeFilter']);
            } else {
                // Requête simple
                $data = $this->getUserSavedSearches();
            }
            $this->view->assignMultiple([
                'userSavedSearches' => $data,
                'agorhaApiUrl' => $this->getApiUrl(),
                'searchUrl' => $this->getSearchUrl(),
                'savedSearchesNumberPerPage' => $savedSearchesNumberPerPage
            ]);
        } else {
            // Si ce n'est pas le cas : redirection vers la page Accueil
            $this->redirectToPage($this->uidPage['homepage'], 302);
        }
    }


    private function getUserSavedSearches($filter = null) {
        try {
            $url = $this->getApiUrl() . '/users/savedSearches?terms='.$filter.'&page=1&pageSize=10000';
            $response = $this->client->get($url, ['cookies' => $this->getCookieJar()]);
        } catch (RequestException $e){}
        if(isset($response) && ($response->getStatusCode() === 200)){
            return \GuzzleHttp\json_decode($response->getBody());
        } else {
            return false;
        }
    }
}
