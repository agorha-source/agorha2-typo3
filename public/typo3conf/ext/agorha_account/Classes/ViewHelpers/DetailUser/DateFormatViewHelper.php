<?php

namespace Sword\AgorhaAccount\ViewHelpers\DetailUser;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class DateFormatViewHelper extends AbstractViewHelper {

  use CompileWithRenderStatic;

  /**
   * @param array $arguments
   * @param \Closure $renderChildrenClosure
   * @param RenderingContextInterface $renderingContext
   * @return string
   */
  public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
    $date = $renderChildrenClosure();
    $date = str_replace('T', ' ', $date);
    return date('d/m/Y H:i',strtotime($date));
  }

}
