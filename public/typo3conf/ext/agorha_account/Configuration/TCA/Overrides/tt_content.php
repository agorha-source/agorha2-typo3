<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaAccount',
    'ListUsers',
    'Liste les comptes utilisateurs',
    'EXT:agorha_base/Resources/Public/Icons/avatar.svg'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaAccount',
    'DetailUser',
    'Récupère les informations de l utilisateur dont l id est renseigné dans l url',
    'EXT:agorha_base/Resources/Public/Icons/avatar.svg'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaAccount',
    'ListResearch',
    'Liste les recherches sauvegardées',
    'EXT:agorha_base/Resources/Public/Icons/loupe.svg'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaaccount_listusers'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaaccount_detailuser'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhaaccount_listresearch'] = 'recursive,select_key,pages';
