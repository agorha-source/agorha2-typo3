
(function () {
  "use strict";

  CKEDITOR.dialog.add( 'noteDialog', function ( editor ) {
    let id;
    return {
      title: 'Note de bas de page',
      minWidth: 400,
      minHeight: 100,

      contents: [
        {
          id: 'tab-basic',
          label: 'Basic Settings',
          elements: [
            {
              type: 'textarea',
              id: 'note',
              label: 'Texte',
              validate: CKEDITOR.dialog.validate.notEmpty( "La note de bas de page ne peut pas être vide" )
            }
          ]
        }
      ],
      onShow: function() {
        let text = editor.getSelection().getStartElement().getHtml();
        let occ = (text.split('note-link').length)-1;
        if(occ !== 0) {
          id = occ + 1;
        } else {
          id = 1;
        }
      },
      onOk: function() {
        let dialog = this;
        let noteValue = dialog.getValueOf('tab-basic', 'note');
        let selection = editor.getSelection();
        var selectedText = selection.getSelectedText();
        // Fait appel à la méthode build du fichier plugin.js
        editor.plugins.note.build(id,noteValue,selectedText,editor);
      }
    };
  });
}) ();
