(function () {
    CKEDITOR.plugins.add( 'note', {
        icons: 'note',
        init: function( editor ) {

            editor.addCommand('note', new CKEDITOR.dialogCommand('noteDialog'));
            CKEDITOR.dialog.add('noteDialog', this.path + 'dialogs/note.js');
            // Insertion du plugin dans la barre de bouton
            editor.ui.addButton( 'note', {
                label: 'Ajouter une note de bas de page',
                command: 'note',
                toolbar: 'insert,100'
            });

          // Permet de réindexer les footnote à chaque changement du dom
          editor.on('change', function() {
            const footnotes = editor.document.$.getElementsByClassName('footnote');
            let footnoteId = 0;
            for (footnoteId; footnoteId < footnotes.length; footnoteId++) {
              footnotes[footnoteId].id = 'note-link-'+ (footnoteId+1);
              footnotes[footnoteId].innerText = footnoteId+1;
              footnotes[footnoteId].hash = 'note-name-' + (footnoteId+1);
            }
          });
        },

      // Crée et insert la note de bas de page dans le texte
      build: function(id,noteValue,selectedText,editor) {
        let footnote = '<sup><a href="#note-name-'+id+'" class="footnote" id="note-link-'+id+'" title="'+noteValue +'">'+id+'</a>';
        if(selectedText) {
          editor.insertHtml(selectedText+footnote);
        } else {
          editor.insertHtml(footnote);
        }
      },
    });
}) ();


