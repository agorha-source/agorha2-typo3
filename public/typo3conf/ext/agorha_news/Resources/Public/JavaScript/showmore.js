/**
 * Met à jour les fonctions d'ouvertures et fermetures des div sur le lien "voir plus"
 */
$(document).ready(function() {
  if (document.getElementById("newsContent")) {
    activShowMoreFunction();
  }
  if (document.getElementById("databasePage")) {
    activShowMoreFunction();
  }
});


function activShowMoreFunction() {
  $(".show-more a").on("click", function() {
    let $this = $(this);
    let $content = $this.parent().prev("div.content");
    let linkText = $this.text().toUpperCase();
    if(linkText === "VOIR PLUS"){
      linkText = "Voir moins";
      $content.toggleClass("hideContent", "showContent", 400);
    } else {
      linkText = "Voir plus";
      $content.toggleClass("hideContent", "showContent", 400);
    }
    $this.text(linkText);
  })
}
