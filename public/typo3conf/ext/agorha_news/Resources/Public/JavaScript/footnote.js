/**
 *
 * Remet à jour les notes de bas de page est les affiche
 */
$(document).ready(function() {
  if (document.getElementById("newsContent")) {
    let footnotes = reindexFootnotesId();
    insertFootnotesInDiv(footnotes);

    $('a[href*="#"]:not([href$="#"])').click(function() {
      // On sort les liens des nav biographie et bibliogaphie
      if(this.id !== 'bibiographie-tab' && this.id !== 'biographie-tab') {
        // Open Footnote tab
        $('#myTab li:first-child a').tab('show')
        if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'')
          && location.hostname === this.hostname
          && $(this).data('toggle') === undefined
          && $(this).data('slide') === undefined) {
          var $target = $(this.hash);
          $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
          if ($target.length) {
            var targetOffset = $target.offset().top;
            var navbar = $('.navbar-fixed-top');
            if(navbar.length && targetOffset !== 0){
              targetOffset -= navbar.outerHeight();
            }

            $('html,body').animate({scrollTop: targetOffset}, 500);
            return false;
          }
        }
      }
    });

  }
});

/**
 * Récupère les notes dans la page et les réindex
 */
function reindexFootnotesId() {
  let footnoteId = 0;
  let footnotes = getFootnotes();
  if (footnotes) {
    for (footnoteId; footnoteId < footnotes.length; footnoteId++) {
      footnotes[footnoteId].id = 'note-link-'+ (footnoteId+1);
      footnotes[footnoteId].innerText = footnoteId+1;
      footnotes[footnoteId].hash = 'note-name' + (footnoteId+1);
    }
    return footnotes;
  }
}

/**
 * Ajoute les notes de bas de page dans la div footnotes
 */
function insertFootnotesInDiv(footnotes) {
  if (footnotes) {
    for(let i=0;i<footnotes.length;i++) {
      $("#footnotes").append('<a href="#'+footnotes[i].id+'" id="note-name'+[i+1]+'">'+[i+1]+'. '+footnotes[i].title+'</a>');
    }
    if(footnotes.length === 0) {
      $("#footnote-tab").css("display", "none");
    }
  }
}

function getFootnotes() {
  return document.getElementsByClassName('footnote');
}
