
function shareNews() {
  let dummy = document.createElement("input");
  document.body.appendChild(dummy);
  dummy.setAttribute("id", "dummy_id");
  let getUrl = window.location.href;
  document.getElementById("dummy_id").value = getUrl;
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);

  let toast = $('.toast');
  toast.toast({
    delay: 5000,
    autohide: true,
    variant: "success"
  });
  toast.toast('show');
}
