<?php
defined('TYPO3_MODE') or die();

$boot = function () {

// Surcharge des fichiers de traduction
  $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']
  ['EXT:news/Resources/Private/Language/locallang_modadministration.xlf'][] = 'EXT:agorha_news/Resources/Private/Language/locallang_modadministration.xlf';

  $GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']
  ['EXT:news/Resources/Private/Language/locallang_be.xlf'][] = 'EXT:agorha_news/Resources/Private/Language/locallang_be.xlf';

// Extension du modèle News et de son repository
  $GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/News'][] = 'agorha_news';
  $GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Repository/NewsRepository'][] = 'agorha_news';

// Extension du modèle TtContent (nécessaire pour l'indexation - mksearch)
  $GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/TtContent'][] = 'agorha_news';

// Permet de rajouter le filtre sur article et actualité dans le module
  $GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/Dto/AdministrationDemand'][] = 'agorha_news';

// XClass AdministrationController
  $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']
  ['GeorgRinger\\News\\Controller\\AdministrationController'] = array('className' => 'Sword\\AgorhaNews\\Controller\\AdministrationController');
// XClass NewsController
  $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']
  ['GeorgRinger\\News\\Controller\\NewsController'] = array('className' => 'Sword\\AgorhaNews\\Controller\\NewsController');

// Edit restriction for news records
  $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] =
    'Sword\\AgorhaNews\\Hooks\\DataHandler';
  $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][] =
    'Sword\\AgorhaNews\\Hooks\\DataHandler';

  /* ===========================================================================
          Modification CKEditor pour les notes de bas de page
      =========================================================================== */
  $GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['news_config'] = 'EXT:agorha_news/Configuration/RTE/newsRTE.yaml';

  /* ===========================================================================
          Add TSconfig
      =========================================================================== */
  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    <INCLUDE_TYPOSCRIPT: source="FILE:EXT:agorha_news/Configuration/TsConfig/Page/All.tsconfig">
    ');

  // Hide content elements in list module & filter in administration module
  $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList::class]['modifyQuery'][]
    = \Sword\AgorhaNews\Hooks\Backend\RecordListQueryHook::class;

  /* ===========================================================================
  Déclaration du plugin AgorhaNews - Permet de récupérer un article et de l'afficher
    =========================================================================== */
  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Sword.AgorhaNews',
    'AgorhaNews',
    [
      'AgorhaNews' => 'index'
    ]
  );
};

// mksearch Evenements Indexer
if (tx_rnbase_util_Extensions::isLoaded('agorha_news')) {
  tx_mksearch_util_Config::registerIndexer(
    'agorha_news',
    'news',
    'tx_mksearch_indexer_AgorhaNews',
    [
      // table principale
      'tx_news_domain_model_news',
      // tables liées à 'écouter'
      'tx_news_domain_model_tag'
    ]
  );
}
$boot();
unset($boot);
