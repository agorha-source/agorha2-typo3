<?php

namespace Sword\AgorhaNews\Hooks;

use Sword\AgorhaNews\Utility\NewsUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class DataHandler extends NewsUtility {

  /**
   * Prevent saving of a news record if the editor doesn't have access to all categories of the news record
   *
   * @param array $fieldArray
   * @param string $table
   * @param int $id
   * @param $parentObject \TYPO3\CMS\Core\DataHandling\DataHandler
   */
  public function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, $parentObject)
  {
    if ($table === 'tx_news_domain_model_news') {
      // Liaison bases de données obligatoire si l'user n'est pas admin
      if(empty($fieldArray['related_pages']) && !$this->getBackendUser()->isAdmin()){
        $parentObject->log($table, $id, 2,0,1,"Vous devez lier une base de données avant de valider",1,[$table]);
        $fieldArray = [];
      }
      // News proposé - ne peut plus être modifié par un contributeur
      if ($this->getHigherUserGroups() === 'CONTRIB' && $this->getNewsStatus($id) != 0) {
        $parentObject->log($table, $id, 2, 0, 1, "Vous n'avez pas les droits requis pour modifier cet élément", 1, [$table]);
        $fieldArray = [];
      }
      // Un responsable peut modifier news à l'état "En rédaction" et "Proposé"
      if ($this->getHigherUserGroups() === 'RESPONSABLE' && $this->getNewsStatus($id) != 0 && $this->getNewsStatus($id) != 1) {
        $parentObject->log($table, $id, 2, 0, 1, "Vous n'avez pas les droits requis pour modifier cet élément", 1, [$table]);
        $fieldArray = [];
      }
    }
  }


  /**
   * Prevent deleting/moving of a news record if the editor doesn't have rights
   *
   * @param string $command
   * @param string $table
   * @param int $id
   * @param string $value
   * @param $parentObject \TYPO3\CMS\Core\DataHandling\DataHandler
   */
  public function processCmdmap_preProcess($command, &$table, $id, $value, $parentObject)
  {
    if ($table == 'tx_news_domain_model_news' && $command == 'delete') {
      // Un admin a tous les droits
      if(!$this->getBackendUser()->isAdmin()){
        // Un contributeur peut juste supprimer un élément à l'état "En rédaction" -> statut = 0
        if($this->getHigherUserGroups() == 'CONTRIB' && $this->getNewsStatus($id) != 0) {
          $this->_flash("Vous n'avez pas les droits requis pour supprimer cet élément.", "Suppression interdite", 'ERROR');
          $table = '';
          // Un responsable peut supprimer un élément à l'état "En rédaction" et "Proposé" -> statut = 0 et 1
        }else if($this->getHigherUserGroups() == 'RESPONSABLE' && $this->getNewsStatus($id) != 0 && $this->getNewsStatus($id) != 1) {
          $this->_flash("Vous n'avez pas les droits requis pour supprimer cet élément.", "Suppression interdite", 'ERROR');
          $table = '';
        }
      }
    }
  }

  /**
   * FlashMessage
   *
   * @param string $message
   * @param string $heading
   * @param string $severity
   */
  private function _flash($message, $heading='', $severity = 'INFO'){
    switch($severity){
      case 'OK':
        $severityConst = FlashMessage::OK;
        break;
      case 'WARNING':
        $severityConst = FlashMessage::WARNING;
        break;
      case 'ERROR':
        $severityConst = FlashMessage::ERROR;
        break;
      case 'NOTICE':
        $severityConst = FlashMessage::NOTICE;
        break;
      case 'INFO':
      default:
        $severityConst = FlashMessage::INFO;
    }
    $message = GeneralUtility::makeInstance(FlashMessage::class,
      $message,
      $heading,
      $severityConst,
      true
    );
    $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
    $flashMessageService = $objectManager->get(FlashMessageService::class);
    $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
    $messageQueue->addMessage($message);
  }

  /**
   * Returns the current BE user.
   *
   * @return \TYPO3\CMS\Core\Authentication\BackendUserAuthentication
   */
  protected function getBackendUser()
  {
    return $GLOBALS['BE_USER'];
  }

}
