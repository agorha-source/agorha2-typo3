<?php

namespace Sword\AgorhaNews\Hooks\Backend;

use Sword\AgorhaNews\Backend\RecordList\RecordListConstraint;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Permet de rajouter le type (articles ou actualités) dans les filtres
 *
 */
class RecordListQueryHook extends \GeorgRinger\News\Hooks\Backend\RecordListQueryHook
{
    public function __construct()
    {
        $this->recordListConstraint = GeneralUtility::makeInstance(\Sword\AgorhaNews\Backend\RecordList\RecordListConstraint::class);
    }
}
