<?php

namespace Sword\AgorhaNews\UserFunc;

use Sword\AgorhaNews\Utility\NewsUtility;

class TcaProcFunc extends NewsUtility {

    /**
     * Permet de modifier le select si l'utilisateur est admin
     * Seul un admin peut publier un article ou une actualité
     *
     * @param array $config
     * @return array
     */
    public function publishSelectIfUserIsAdmin($config)
    {
        if($GLOBALS['BE_USER']->isAdmin() || $this->getHigherUserGroups() === 'RESPONSABLE') {
            $config['items'] = [
                ['En rédaction',0],
                ['Proposer', 1],
                ['Publier', 2],
                ['Dé-publier',4],
                ['Archiver',3]
            ];
        } else {
            $config['items'] = [
                ['En rédaction',0],
                ['Proposer', 1],
                ['Archiver',3]
            ];
        }
        return $config;
    }

  /**
   * Permet de créer un article ou une actualité selon le dossier
   * dans lequel l'utilisateur est positionné
   *
   * 52 -> dossier Articles
   * 53 -> dossier Actualités
   *
   * @param $config
   */
    public function setTypeAccordingToBackendPage($config)
    {
      $pageId = $config['row']['pid'];
      if($pageId == 53) {
        $config['items'] = [
          ['Actualité', 'Actualités', 'ext-news-type-default']
        ];
      } else {
        $config['items'] = [
          ['Article', 'Articles', 'ext-news-type-default']
        ];
      }
    }
}
