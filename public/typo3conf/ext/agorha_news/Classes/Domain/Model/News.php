<?php

namespace Sword\AgorhaNews\Domain\Model;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * News model
 */
class News extends \GeorgRinger\News\Domain\Model\News
{

 /**
   * @var string
   */
  protected $bibliographieTitle;

  /**
   * @var string
   */
  protected $bibliographie;

/**
   * @var string
   */
  protected $biographieTitle;

  /**
   * @var string
   */
  protected $biographie;

  /**
   * @var string
   */
  protected $newsDescription;

  /**
   * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sword\AgorhaNews\Domain\Model\Pages>
   * @lazy
   */
  protected $relatedPages;

  /**
   * Header media
   *
   * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\News\Domain\Model\FileReference>
   * @lazy
   */
  protected $headerMedia;

  /**
   * @var int
   */
  protected $status;

  /**
   * @var string
   */
  protected $agorhaNewsType;

  /**
   * Initialize pages(databases) and header media + parent construct
   *
   */
  public function __construct()
  {
    parent::__construct();
    $this->relatedPages = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    $this->headerMedia = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
  }

  /**
     * Get Bibliographie Title
     *
     * @return string
     */
    public function getBibliographieTitle()
    {
      return $this->bibliographieTitle;
    }

/**
   * Set Bibliographie Title
   *
   * @param $bibliographieTitle string bibliographieTitle
   */
  public function setBibliographieTitle($bibliographieTitle)
  {
    $this->bibliographieTitle = $bibliographieTitle;
  }

  /**
   * Get Bibliographie
   *
   * @return string
   */
  public function getBibliographie()
  {
    return $this->bibliographie;
  }

  /**
   * Set Bibliographie
   *
   * @param $bibliographie string bibliographie
   */
  public function setBibliographie($bibliographie)
  {
    $this->bibliographie = $bibliographie;
  }

/**
   * Get Biographie
   *
   * @return string
   */
  public function getBiographieTitle()
  {
    return $this->biographieTitle;
  }

  /**
     * Set Biographie Title
     *
     * @param $biographieTitle string biographieTitle
     */
    public function setBiographieTitle($biographieTitle)
    {
      $this->biographieTitle = $biographieTitle;
    }

  /**
   * Get Biographie
   *
   * @return string
   */
  public function getBiographie()
  {
    return $this->biographie;
  }

  /**
   * Set Biographie
   *
   * @param $biographie string biographie
   */
  public function setBiographie($biographie)
  {
    $this->biographie = $biographie;
  }

  /**
   * Get News Description
   *
   * @return string
   */
  public function getNewsDescription()
  {
    return $this->newsDescription;
  }

  /**
   * Set News Description
   *
   * @param $newsDescription
   */
  public function setNewsDescription($newsDescription)
  {
    $this->newsDescription = $newsDescription;
  }

  /**
   * Get Related Pages
   *
   * @return ObjectStorage
   */
  public function getRelatedPages()
  {
    return $this->relatedPages;
  }

  /**
   * Set related pages (databases)
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Sword\AgorhaNews\Domain\Model\Pages> $relatedPages related pages
   */
  public function setRelatedPages($relatedPages)
  {
    $this->relatedPages = $relatedPages;
  }

  /**
   * Get header media
   *
   * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
   */
  public function getHeaderMedia()
  {
    return $this->headerMedia;
  }

  /**
   * Set header media
   *
   * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $headerMedia
   */
  public function setHeaderMedia(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $headerMedia)
  {
    $this->headerMedia = $headerMedia;
  }

  /**
   * Get status
   *
   * @return int
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Set status
   *
   * @param $status int status
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * @return string
   */
  public function getAgorhaNewsType()
  {
    return $this->agorhaNewsType;
  }

  /**
   * @param string $agorhaNewsType
   */
  public function setAgorhaNewsType(string $agorhaNewsType)
  {
    $this->agorhaNewsType = $agorhaNewsType;
  }

}
