<?php

namespace Sword\AgorhaNews\Domain\Model;


/**
 * Tt content model
 */
class TtContent extends \GeorgRinger\News\Domain\Model\TtContent
{
  /**
   * @var string
   */
    protected $txMaskTitle;

  /**
   * @var string
   */
    protected $txMaskNoticeSelection;

  /**
   * Get tx mask title
   * @return string
   */
    public function getTxMaskTitle() {
      return $this->txMaskTitle;
    }

  /**
   * Set tx mask title
   * @param $txMaskTitle
   */
    public function setTxMaskTitle($txMaskTitle) {
      $this->txMaskTitle = $txMaskTitle;
    }

  /**
   * Get tx mask notice selection
   * @return mixed
   */
    public function getTxMaskNoticeSelection() {
      return $this->txMaskNoticeSelection;
    }

  /**
   * Set tx mask notice selection
   * @param $txMaskNoticeSelection
   */
    public function setTxMaskNoticeSelection($txMaskNoticeSelection) {
      $this->txMaskNoticeSelection = $txMaskNoticeSelection;
    }
}
