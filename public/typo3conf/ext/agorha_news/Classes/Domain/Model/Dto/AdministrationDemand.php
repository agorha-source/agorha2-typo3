<?php
namespace Sword\AgorhaNews\Domain\Model\Dto;

/**
 * This file is part of the "news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

/**
 * Administration Demand model
 */
class AdministrationDemand extends \GeorgRinger\News\Domain\Model\Dto\AdministrationDemand
{

    /**
     * @var int
     */
    protected $type;

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}
