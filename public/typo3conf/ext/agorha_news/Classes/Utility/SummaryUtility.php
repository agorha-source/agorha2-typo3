<?php

namespace Sword\AgorhaNews\Utility;

class SummaryUtility
{

  /**
   * Retourne un sommaire contenant 3 niveaux de titres
   * @param \GeorgRinger\News\Domain\Model\News $news
   * @return array
   */
  public function getSummary(\GeorgRinger\News\Domain\Model\News $news) {
    $elements = $news->getContentElements();
    $summary = [];
    if ($elements) {
      for ($i = 0; $i < count($elements); $i++) {
        // Si le composant a un titre on le récupère
        if ($elements[$i]->getHeader()) {
          $summary[$i]['header'] = '<li class="level-2">' . $elements[$i]->getHeader() . '</li>';
        } else if ($elements[$i]->getTxMaskTitle()){
          $summary[$i]['header'] = '<li class="level-2">' . $elements[$i]->getTxMaskTitle() . '</li>';
        }
        // Si le composant a un bodytext on récupère les balises H3 et H4
        if ($elements[$i]->getBodytext()) {
          $marksFromBodytext = $this->getMarksFromBodytext($elements[$i]->getBodytext());
          for($j=0;$j<count($marksFromBodytext[0]);$j++) {
            if(strpos($marksFromBodytext[0][$j],'h3')) {
              $summary[$i]['subheader'][$j] = '<li class="level-3">' . $marksFromBodytext[1][$j] . '</li>';
            } else {
              $summary[$i]['subheader'][$j] = '<li class="level-4">' . $marksFromBodytext[1][$j] . '</li>';
            }
          }
        }
        if($summary[$i]) {
          $summary[$i]['target'] = '#'.$elements[$i]->getUid();
        }
      }
    }
    return $summary;
  }

  /**
   * On construit le sommaire de l'article avec le titre - sous-titre et sous-titres présents dans le bodytext
   * @param $news
   */
  public function getBodyTextWithAnchors($news)
  {
    $elements = $news->getContentElements();
    if ($elements) {
      for ($i = 0; $i < count($elements); $i++) {
        $uid = $elements[$i]->getUid();
        // Si le composant a un titre on le récupère
        if ($elements[$i]->getHeader()) {
          $elements[$i]->setHeader('<h2 id='.$uid.'-header>'.$elements[$i]->getHeader().'</h2>');
        } else if ($elements[$i]->getTxMaskTitle()){
          $elements[$i]->setTxMaskTitle('<h2 id='.$uid.'-header>'.$elements[$i]->getTxMaskTitle().'</h2>');
        }
        if ($elements[$i]->getBodytext()) {
          // On récupère les balises H3 et on leur rajoute une ancre
          $bodytextWithAnchors = $this->getMarksAndSetAnchors($elements[$i]->getBodytext(),$uid);
          // Puis on récupère les balises H4 et on leur rajoute une ancre
          $elements[$i]->setBodyText($bodytextWithAnchors);
        }
      }
    }
    return $elements;
  }

  /**
   * Trouve toutes les occurences d'une balise et la modifie pour y rajouter une ancre
   * @param $delimiter
   * @param $bodytext
   * @param $uid
   * @return string
   */
  private function getMarksAndSetAnchors($bodytext,$uid)
  {
    // Scinde une chaîne de caractères en segments ne contenant qu'une occurence de la balise voulue
    $globalArray = $this->getH3AndH4InGlobalArray($bodytext);
    return $this->setAnchorsOnH3AndH4Marks($globalArray,$uid);
  }

  private function getH3AndH4InGlobalArray($bodytext) {
    // On explode le corps de texte par rapport aux h3
    $segments = explode('</h3>', $bodytext);
    $globalArray = [];
    for ($i = 0; $i < count($segments); $i++) {
      // Puis on explode avec les h4
      $data = explode('</h4>', $segments[$i]);
      if (is_array($data) && count($data) > 1) {
        for ($j = 0; $j < count($data); $j++) {
          array_push($globalArray, $data[$j]);
        }
      } else {
        array_push($globalArray, $data[0]);
      }
    }
    return $globalArray;
  }

  private function setAnchorsOnH3AndH4Marks($globalArray,$uid) {
    $bodytextWithAnchors = '';
    $tagToSearch = null;
    $marks = null;
    for($i=0;$i<count($globalArray);$i++) {
      // On vérifie la présence de cet index du tableau
      if (strpos($globalArray[$i], ('<h3>')) !== false) {
        $tagToSearch = '<h3>';
        $marks = 'h3';
      } else if((strpos($globalArray[$i], ('<h4>')) !== false)) {
        $tagToSearch = '<h4>';
        $marks = 'h4';
      }
      if($tagToSearch && $marks) {
        // On rajoute un id à la balise
        $tagToReplace = '<'.$marks .' id='.$uid.'-subheader-'.$i.'>';
        $globalArray[$i] = str_replace($tagToSearch, $tagToReplace, $globalArray[$i]);
        // On rajoute la balise fermée à l'index car explode la supprime
        $globalArray[$i] .= '</'.$marks.'>';
      }
      // On concatène le tableau pour retourner une string
      $bodytextWithAnchors .= $globalArray[$i];
    }
    return $bodytextWithAnchors;
  }

  /**
   * Récupère toutes les balises {marks} présentes dans bodytext
   * @param $bodytext
   * @return mixed
   */
  private function getMarksFromBodytext($bodytext)
  {
    preg_match_all('#<h[3.4][^>]*>(.*?)<\/h[3.4]>#i', $bodytext, $subTitles);
    return $subTitles;
  }

}
