<?php

namespace Sword\AgorhaNews\Utility;

class NewsUtility {

  protected function getNewsStatus($id) {
      $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
        ->getQueryBuilderForTable('tx_news_domain_model_news');
      $statement = $queryBuilder
        ->select('status')
        ->from('tx_news_domain_model_news')
        ->where(
          $queryBuilder->expr()->eq('tx_news_domain_model_news.uid', $queryBuilder->createNamedParameter($id, \PDO::PARAM_INT))
        )
        ->execute();
      return $statement->fetch()['status'];
  }

  /**
   * @return string
   */
  protected function getHigherUserGroups()
  {
    if ($GLOBALS['BE_USER']->isAdmin()) {
      return "ADMIN";
    }
    $userGroups = $GLOBALS['BE_USER']->userGroups;
    if($userGroups[2]){
      return "CONTRIB";
    } else {
      return "RESPONSABLE";
    }
  }

  /**
   * Récupère les informations de l'image d'en tête
   * @param $news
   * @return null
   */
  public function getHeaderMedia($news) {
    $mediaFile = $news->getHeaderMedia();
    if($mediaFile) {
      return $mediaFile[0]->getOriginalResource();
    }
    return null;
  }



}
