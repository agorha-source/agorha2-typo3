<?php

namespace Sword\AgorhaNews\Controller;

use GeorgRinger\News\Utility\Cache;
use GeorgRinger\News\Utility\Page;

class NewsController extends \GeorgRinger\News\Controller\NewsController
{

    /**
     * Single view of a news record
     *
     * Controller utilisé dans la prévisualisation des news
     *
     * @param \GeorgRinger\News\Domain\Model\News $news news item
     * @param int $currentPage current page for optional pagination
     */
    public function detailAction(\GeorgRinger\News\Domain\Model\News $news = null, $currentPage = 1)
    {
      // On intercepte l'id de la news pour récupérer ses infos
        if ($news === null || $this->settings['isShortcut']) {
            $previewNewsId = ((int)$this->settings['singleNews'] > 0) ? $this->settings['singleNews'] : 0;
            if ($this->request->hasArgument('news_preview')) {
                $previewNewsId = (int)$this->request->getArgument('news_preview');
            }
            if ($previewNewsId > 0) {
                if ($this->isPreviewOfHiddenRecordsEnabled()) {
                    $GLOBALS['TSFE']->showHiddenRecords = true;
                    $news = $this->newsRepository->findByUid($previewNewsId, false);
                } else {
                    $news = $this->newsRepository->findByUid($previewNewsId);
                }
            }
        }

        if (is_a($news,
                'GeorgRinger\\News\\Domain\\Model\\News') && $this->settings['detail']['checkPidOfNewsRecord']
        ) {
            $news = $this->checkPidOfNewsRecord($news);
        }

        $demand = $this->createDemandObjectFromSettings($this->settings);
        $demand->setActionAndClass(__METHOD__, __CLASS__);

        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        // Récupération du sommaire
        $summaryUtility = new \Sword\AgorhaNews\Utility\SummaryUtility();
        $summary = $summaryUtility->getSummary($news);

        // Récupération le corps de texte avec les ancres pour le sommaire
        $contentElementsWithMarks = $summaryUtility->getBodyTextWithAnchors($news);

        // Récupère l'image d'en tête
        $newsUtility = new \Sword\AgorhaNews\Utility\NewsUtility();
        $headerMedia = $newsUtility->getHeaderMedia($news);

        $assignedValues = [
            'now' => date('d/m/Y'),
            'link' => $link,
            'headerFile' => $headerMedia,
            'newsItem' => $news,
            'currentPage' => (int)$currentPage,
            'demand' => $demand,
            'settings' => $this->settings,
            'summary' => $summary,
            'contentElements' => $contentElementsWithMarks
        ];

        $assignedValues = $this->emitActionSignal('NewsController', self::SIGNAL_NEWS_DETAIL_ACTION, $assignedValues);
        $news = $assignedValues['newsItem'];

        $this->view->assignMultiple($assignedValues);

        Page::setRegisterProperties($this->settings['detail']['registerProperties'], $news);
        if (!is_null($news) && is_a($news, 'GeorgRinger\\News\\Domain\\Model\\News')) {
            Cache::addCacheTagsByNewsRecords([$news]);
        }
    }


}
