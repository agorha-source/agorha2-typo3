<?php

namespace Sword\AgorhaNews\Controller;

use GeorgRinger\News\Domain\Model\News;
use Sword\AgorhaBase\Controller\UtilityController;

class AgorhaNewsController extends UtilityController
{

  /**
   * Controller utilisé dans le plugin de détail d'un élément (article ou actu)
   * On récupère l'id présent dans l'url et on retourne ses infos
   * @param News $news
   */
  public function indexAction(News $news)
  {
    if (isset($news) && ($news->getStatus() === 2 || $this->hasRights())) {
      $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

      // Récupération du sommaire
      $summaryUtility = new \Sword\AgorhaNews\Utility\SummaryUtility();
      $summary = $summaryUtility->getSummary($news);

      // Récupération le corps de texte avec les ancres pour le sommaire
      $contentElementsWithMarks = $summaryUtility->getBodyTextWithAnchors($news);

      // Récupère l'image d'en tête
      $newsUtility = new \Sword\AgorhaNews\Utility\NewsUtility();
      $headerMedia = $newsUtility->getHeaderMedia($news);

      $assignedValues = [
        'now' => date('d/m/Y'),
        'link' => $link,
        'headerFile' => $headerMedia,
        'newsItem' => $news,
        'summary' => $summary,
        'contentElements' => $contentElementsWithMarks
      ];
      $this->view->assignMultiple($assignedValues);
    } else {
      // Redirection page accueil si on arrive sur la page sans l'uid d'un élément dans l'url
      $this->redirectToPage($this->uidPage['homepage'], 303);
    }
  }
}
