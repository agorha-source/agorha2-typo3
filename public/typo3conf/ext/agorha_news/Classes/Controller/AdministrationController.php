<?php

namespace Sword\AgorhaNews\Controller;

use GeorgRinger\News\Backend\RecordList\NewsDatabaseRecordList;
use GeorgRinger\News\Domain\Model\Dto\AdministrationDemand;
use TYPO3\CMS\Backend\Clipboard\Clipboard;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Utility\BackendUtility as BackendUtilityCore;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

/**
 * Administration controller
 */
class AdministrationController extends \GeorgRinger\News\Controller\AdministrationController
{
    // Suppression du menu déroulant dans module
    protected function createMenu()
    {
    }

    // Delete support area
    private function showSupportArea()
    {
    }

    // Delete donate action
    public function donateAction()
    {
    }


    /**
     * Create the panel of buttons
     *
     */
    protected function createButtons()
    {
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);

        $buttons = [];

        // Si on est pas dans le dossier articles ou actualités - pas de boutons nouvel article / actu
        if ($this->pageUid == $this->tsConfiguration['folders.']['article'] || $this->pageUid == $this->tsConfiguration['folders.']['actu']) {
            if ($this->request->getControllerActionName() === 'index' && $this->isFilteringEnabled()) {
                $toggleButton = $buttonBar->makeLinkButton()
                    ->setHref('#')
                    ->setDataAttributes([
                        'togglelink' => '1',
                        'toggle' => 'tooltip',
                        'placement' => 'bottom',
                    ])
                    ->setTitle($this->getLanguageService()->sL('LLL:EXT:agorha_news/Resources/Private/Language/locallang_be.xlf:administration.toggleForm'))
                    ->setIcon($this->iconFactory->getIcon('actions-filter', Icon::SIZE_SMALL));
                $buttonBar->addButton($toggleButton, ButtonBar::BUTTON_POSITION_LEFT, 1);
            }
            $buttons = [
                [
                    'table' => 'tx_news_domain_model_news',
                    'label' => 'module.createNewNewsRecord',
                    'action' => 'newNews',
                    'icon' => 'ext-news-type-default'
                ]
            ];
        }

        foreach ($buttons as $key => $tableConfiguration) {
            if ($this->showButton($tableConfiguration['table'])) {
                $title = $this->getLanguageService()->sL('LLL:EXT:agorha_news/Resources/Private/Language/locallang_be.xlf:' . $tableConfiguration['label']);
                $viewButton = $buttonBar->makeLinkButton()
                    ->setHref($uriBuilder->reset()->setRequest($this->request)->uriFor($tableConfiguration['action'],
                        [], 'Administration'))
                    ->setDataAttributes([
                        'toggle' => 'tooltip',
                        'placement' => 'bottom',
                        'title' => $title])
                    ->setTitle($title)
                    ->setIcon($this->iconFactory->getIcon($tableConfiguration['icon'], Icon::SIZE_SMALL, 'overlay-new'));
                $buttonBar->addButton($viewButton, ButtonBar::BUTTON_POSITION_LEFT, 2);
            }
        }

        $clipBoard = GeneralUtility::makeInstance(Clipboard::class);
        $clipBoard->initializeClipboard();
        $elFromTable = $clipBoard->elFromTable('tx_news_domain_model_news');
        if (!empty($elFromTable)) {
            $viewButton = $buttonBar->makeLinkButton()
                ->setHref($clipBoard->pasteUrl('', $this->pageUid))
                ->setOnClick('return ' . $clipBoard->confirmMsgText('pages',
                        BackendUtilityCore::getRecord('pages', $this->pageUid), 'into',
                        $elFromTable))
                ->setTitle($this->getLanguageService()->sL('LLL:EXT:lang/Resources/Private/Language/locallang_mod_web_list.xlf:clip_pasteInto'))
                ->setIcon($this->iconFactory->getIcon('actions-document-paste-into', Icon::SIZE_SMALL));
            $buttonBar->addButton($viewButton, ButtonBar::BUTTON_POSITION_LEFT, 4);
        }

        // Refresh
        $refreshButton = $buttonBar->makeLinkButton()
            ->setHref(GeneralUtility::getIndpEnv('REQUEST_URI'))
            ->setTitle($this->getLanguageService()->sL('LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.reload'))
            ->setIcon($this->iconFactory->getIcon('actions-refresh', Icon::SIZE_SMALL));
        $buttonBar->addButton($refreshButton, ButtonBar::BUTTON_POSITION_RIGHT);

        // Shortcut
        if ($this->getBackendUser()->mayMakeShortcut()) {
            $shortcutButton = $buttonBar->makeShortcutButton()
                ->setModuleName('web_NewsTxNewsM2')
                ->setGetVariables(['route', 'module', 'id'])
                ->setDisplayName('Shortcut');
            $buttonBar->addButton($shortcutButton, ButtonBar::BUTTON_POSITION_RIGHT);
        }
    }

    /**
     * Main action for administration
     */
    public function indexAction()
    {
        $this->redirectToPageOnStart();

        $demandVars = GeneralUtility::_GET('tx_news_web_newstxnewsm2');
        $demand = $this->objectManager->get(AdministrationDemand::class);
        $autoSubmitForm = 0;
        if (is_array($demandVars['demand'])) {
            foreach ($demandVars['demand'] as $key => $value) {
                if (property_exists(AdministrationDemand::class, $key)) {
                    $getter = 'set' . ucfirst($key);
                    $demand->$getter($value);
                }
            }
        } else {
            // Preselect by TsConfig (e.g. tx_news.module.preselect.topNewsRestriction = 1)
            if (isset($this->tsConfiguration['preselect.'])
                && is_array($this->tsConfiguration['preselect.'])
            ) {
                $anyPropertySet = false;
                unset($this->tsConfiguration['preselect.']['orderByAllowed']);

                foreach ($this->tsConfiguration['preselect.'] as $propertyName => $propertyValue) {
                    $propertySet = ObjectAccess::setProperty($demand, $propertyName, $propertyValue);
                    if ($propertySet) {
                        $anyPropertySet = true;
                    }
                }

                if ($anyPropertySet && !GeneralUtility::_GET('formSubmitted')) {
                    $autoSubmitForm = 1;
                }
            }
            if (!(bool)$this->tsConfiguration['alwaysShowFilter'] || !$this->isFilteringEnabled()) {
                $this->view->assign('hideForm', true);
            }
        }
        $this->view->assign('autoSubmitForm', $autoSubmitForm);

        $categories = $this->categoryRepository->findParentCategoriesByPid($this->pageUid);
        $idList = [];
        foreach ($categories as $c) {
            $idList[] = $c->getUid();
        }
        if (empty($idList) && !$this->getBackendUser()->isAdmin()) {
            $idList = $this->getBackendUser()->getCategoryMountPoints();
        }

        if (!empty($this->tsConfiguration['allowedCategoryRootIds'])) {
            $allowedList = GeneralUtility::intExplode(',', $this->tsConfiguration['allowedCategoryRootIds'], true);
            if (!empty($allowedList)) {
                $idList = array_intersect($idList, $allowedList);
            }
        }

        // Initialize the dblist object:
        $dblist = GeneralUtility::makeInstance(NewsDatabaseRecordList::class);
        $dblist->script = GeneralUtility::getIndpEnv('REQUEST_URI');
        $dblist->thumbs = $this->getBackendUser()->uc['thumbnailsByDefault'];
        $dblist->allFields = 1;
        $dblist->localizationView = $this->tsConfiguration['localizationView'] ? MathUtility::forceIntegerInRange($this->tsConfiguration['localizationView'], 0) : 1;
        $dblist->clickTitleMode = 'edit';
        $dblist->calcPerms = $this->getBackendUser()->calcPerms($this->pageInformation);
        $dblist->showClipboard = 0;
        $dblist->disableSingleTableView = 1;
        $dblist->pageRow = $this->pageInformation;
        $dblist->displayFields = false;
        $dblist->dontShowClipControlPanels = true;
        $dblist->counter++;
        $dblist->MOD_MENU = ['bigControlPanel' => '', 'clipBoard' => '', 'localization' => ''];
        $pointer = MathUtility::forceIntegerInRange(GeneralUtility::_GP('pointer'), 0);
        $limit = isset($this->settings['list']['paginate']['itemsPerPage']) ? (int)$this->settings['list']['paginate']['itemsPerPage'] : 20;
        $dblist->start($this->pageUid, 'tx_news_domain_model_news', $pointer, '',
            $demand->getRecursive(), $limit);
        $dblist->setDispFields();

        $dblist->noControlPanels = !(bool)$this->tsConfiguration['controlPanels'];
        $dblist->setFields = [
            'tx_news_domain_model_news' => GeneralUtility::trimExplode(',', $this->tsConfiguration['columns'] ?: 'author,status,datetime', true)
        ];

        $dblist->script = $_SERVER['REQUEST_URI'];
        $dblist->generateList();

        $assignedValues = [
            'moduleToken' => $this->getToken(true),
            'page' => $this->pageUid,
            'demand' => $demand,
            'news' => $dblist->HTMLcode,
            'newsCount' => $dblist->counter,
            'showSearchForm' => (!is_null($demand) || $dblist->counter > 0),
            'requestUri' => GeneralUtility::quoteJSvalue(rawurlencode(GeneralUtility::getIndpEnv('REQUEST_URI'))),
            'categories' => $this->categoryRepository->findTree($idList),
            'filters' => $this->tsConfiguration['filters.'],
            'folders' => $this->tsConfiguration['folders.'],
            'enableFiltering' => $this->isFilteringEnabled(),
            'dateformat' => $GLOBALS['TYPO3_CONF_VARS']['SYS']['ddmmyy']
        ];

        $assignedValues = $this->emitActionSignal('AdministrationController', self::SIGNAL_ADMINISTRATION_INDEX_ACTION,
            $assignedValues);
        $this->view->assignMultiple($assignedValues);
    }


}
