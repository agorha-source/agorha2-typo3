<?php

namespace Sword\AgorhaNews\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

class CountCharactersViewHelper extends AbstractViewHelper {
  use CompileWithRenderStatic;

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('maxCharacters', 'number', 'Caractères max');
  }

  /**
   * @param array $arguments
   * @param \Closure $renderChildrenClosure
   * @param RenderingContextInterface $renderingContext
   */
  public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
    $stringToCount = $renderChildrenClosure();
    if(strlen($stringToCount) > $arguments['maxCharacters']) {
        return true;
    } else {
      return false;
    }
  }

}
