<?php

namespace Sword\AgorhaNews\ViewHelpers;

use GeorgRinger\News\Domain\Model\Tag;
use Sword\AgorhaBase\Utility\FacetsUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class TagsToFacetsViewHelper extends AbstractViewHelper {

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('tag', Tag::class, 'tag thématique');
  }

  /**
   * Permet de transformer un tag thématique au format des facettes de nuxt
   * Utilisé dans le fichier public/typo3conf/ext/agorha_news/Resources/Private/Partials/News/ListButtons.html
   *
   * @return string
   */
  public function render() {
    $facetsUtility = new FacetsUtility();
    $facets["Catégories"] = $facetsUtility->initFacet(
      $this->arguments['tag']->getTitle(),
      "content.tags",
      false,
      '',
      ''
    );
    return $facetsUtility->encodeFacets($facets);
  }


}
