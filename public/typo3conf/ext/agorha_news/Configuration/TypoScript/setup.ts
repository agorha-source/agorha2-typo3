page {
    includeCSS {
        theme-news = EXT:agorha_news/Resources/Public/Scss/theme-news.scss
    }
    includeJSFooter {
        footnote = EXT:agorha_news/Resources/Public/JavaScript/footnote.js
        share = EXT:agorha_news/Resources/Public/JavaScript/share.js
        showmore = EXT:agorha_news/Resources/Public/JavaScript/showmore.js
        summary = EXT:agorha_news/Resources/Public/JavaScript/summary.js
    }

    10 = FLUIDTEMPLATE
    10 {
        layoutRootPaths {
            10 = EXT:agorha_news/Resources/Private/Layouts/Page/
        }

        partialRootPaths {
            10 = EXT:agorha_news/Resources/Private/Partials/Page/
        }

        templateRootPaths {
            10 = EXT:agorha_news/Resources/Private/Templates/Page/
        }
    }

}
config.tx_extbase {
  persistence {
    classes {
      Sword\AgorhaNews\Domain\Model\Pages {
        mapping {
          tableName = pages
        }
      }
    }
  }
}

# Permet de supprimer les balises vides rajoutées par <f:format.html></f:format.html>
tt_content.stdWrap.dataWrap >
lib.parseFunc_RTE.nonTypoTagStdWrap.encapsLines>
