<?php
defined('TYPO3_MODE') or die();

$configuration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\GeorgRinger\News\Domain\Model\Dto\EmConfiguration::class);

$ll = 'LLL:EXT:agorha_news/Resources/Private/Language/locallang_db.xlf:';

/* Ajout des nouvelles entrées dans le TCA */
$additionalColumns['bibliographie_title'] = [
    'exclude' => true,
    'label' => 'Titre de l\'onglet bibliographie',
    'config' => [
        'type' => 'text',
        'size' => 30,
        'enableRichtext' => false,
        'placeholder' => 'Bibliographie de l\'auteur, Bibliographie ...'
    ],
];
$additionalColumns['bibliographie'] = [
    'exclude' => true,
    'label' => 'Bibliographie',
    'config' => [
        'type' => 'text',
        'enableRichtext' => true
    ],
];
$additionalColumns['biographie_title'] = [
    'exclude' => true,
    'label' => 'Titre de l\'onglet biographie',
    'config' => [
        'type' => 'text',
        'size' => 30,
        'enableRichtext' => false,
        'placeholder' => 'Biographie de l\'auteur, Biographie ...'
    ],
];
$additionalColumns['biographie'] = [
    'exclude' => true,
    'label' => 'Biographie',
    'config' => [
        'type' => 'text',
        'enableRichtext' => true
    ],
];
$additionalColumns['news_description'] = [
  'exclude' => false,
  'label' => 'Description - (visible uniquement sur la recherche Articles/Actualités)',
  'config' => [
    'type' => 'text',
    'enableRichtext' => true,
    'eval' => 'required'
  ],
];
/* Ajout des nouvelles entrées dans le TCA */
$additionalColumns['related_pages'] = [
    'exclude' => true,
    'label' => $ll . 'tx_news_domain_model_news.related',
    'config' => [
        'type' => 'group',
        'internal_type' => 'db',
        'allowed' => 'pages',
        'foreign_table' => 'pages',
        'size' => 5,
        'minitems' => 0,
        'maxitems' => 100,
        'MM' => 'tx_news_domain_model_news_related_mm',
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ]
];
$additionalColumns['keywords'] = [
    'exclude' => true,
    'label' => $GLOBALS['TCA']['pages']['columns']['keywords']['label'],
    'config' => [
        'type' => 'text',
        'placeholder' => $ll . 'tx_news_domain_model_news.keywords.placeholder',
        'cols' => 30,
        'rows' => 5,
        'eval' => 'required',
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ]
];
$additionalColumns['description'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.description_formlabel',
    'config' => [
        'type' => 'text',
        'cols' => 30,
        'rows' => 5,
        'eval' => 'required',
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ]
];
/* Ajout des nouvelles entrées dans le TCA */
$additionalColumns['header_media'] = [
    'exclude' => true,
    'label' => $ll . 'tx_news_domain_model_news.header_media',
    'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
        'header_media',
        [
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
            'appearance' => [
                'createNewRelationLinkTitle' => $ll . 'tx_news_domain_model_news.header_media.add',
            ],
            'foreign_match_fields' => [
                'fieldname' => 'header_media',
                'tablenames' => 'tx_news_domain_model_news',
                'table_local' => 'sys_file',
            ],
            // custom configuration for displaying fields in the overlay/reference table
            // to use the newsPalette and imageoverlayPalette instead of the basicoverlayPalette
            'overrideChildTca' => [
                'types' => [
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                        'showitem' => '
                            --palette--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
                            --palette--;;imageoverlayPalette,
                            --palette--;;filePalette'
                    ],
                ],
            ],
            'minitems' => 1,
            'maxitems' => 1
        ],
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
    )
];
$additionalColumns['status'] = [
    'label' => 'Statut',
    'config' => [
        'type' => 'select',
        'itemsProcFunc' => 'Sword\\AgorhaNews\\UserFunc\\TcaProcFunc->publishSelectIfUserIsAdmin',
        'maxitems' => 1
    ],
];
/* Réécriture des types d'objet proposé :
    0 = Article
    1 = Actualité
*/
$additionalColumns['agorha_news_type'] = [
  'exclude' => false,
  'label' => 'Type',
  'config' => [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'itemsProcFunc' => 'Sword\\AgorhaNews\\UserFunc\\TcaProcFunc->setTypeAccordingToBackendPage',
    'size' => 1,
    'maxitems' => 1,
  ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tx_news_domain_model_news',
    $additionalColumns
);

$GLOBALS['TCA']['tx_news_domain_model_news']['columns']['author'] = [
    'exclude' => true,
    'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.author_formlabel',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'required',
        'behaviour' => [
            'allowLanguageSynchronization' => true,
        ],
    ]
];

$GLOBALS['TCA']['tx_news_domain_model_news']['types'] = [
    // Article
    '0' => [
        'showitem' => '
                --palette--;;paletteCore,title,--palette--;;paletteDate,news_description,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media,
                    header_media,
                --div--;' . $ll . 'tx_news_domain_model_news.tabs.relations,
                    related_pages,tags,
                --div--;' . $ll . 'tx_news_domain_model_news.contentelements,
                    content_elements,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.editorial;
                    ,bibliographie_title,bibliographie,biographie_title,biographie,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;paletteAccess,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,'
    ],
    // Actualité
    '1' => [
        'showitem' => '--palette--;;paletteCore,title,--palette--;;paletteDate,news_description,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media,
                    header_media,
                --div--;' . $ll . 'tx_news_domain_model_news.tabs.relations,
                    related_pages,tags,
                --div--;' . $ll . 'tx_news_domain_model_news.contentelements,
                content_elements,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;paletteAccess,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,'
    ]
];
$GLOBALS['TCA']['tx_news_domain_model_news']['palettes'] = [
    'paletteDate' => [
        'showitem' => 'datetime,author,',
    ],
    'paletteCore' => [
        'showitem' => 'agorha_news_type,status,',
    ],
    'metatags' => [
        'showitem' => 'keywords,description,',
    ],
    'alternativeTitles' => [
        'showitem' => 'alternative_title',
    ],
    'paletteAccess' => [
        'label' => $ll . 'palette.access',
        'showitem' => 'hidden,editlock',
    ],
    'paletteSlug' => [
        'showitem' => '
            path_segment
        ',
    ]
];


