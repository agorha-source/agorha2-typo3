<?php

/*
 * This file is part of the package bk2k/bootstrap-package.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/***************
 * Adjust columns for generic usage
 */
$GLOBALS['TCA']['tt_content']['columns']['width'] = [
    'exclude' => true,
    'label' => 'Largeur de l\'image',
    'config' => [
      'type' => 'select',
      'renderType' => 'selectSingle',
      'items' => [
        ['10%', 10],
        ['20%', 20],
        ['30%', 30],
        ['40%', 40],
        ['50%', 50],
        ['60%', 60],
        ['70%', 70],
        ['80%', 80],
        ['90%', 90],
        ['100%', 100]
      ],
    ],
];

/***************
 * Adjust columns for generic usage
 */
$GLOBALS['TCA']['tt_content']['columns']['image_position'] = [
  'exclude' => true,
  'label' => 'Alignement de l\'image',
  'config' => [
    'type' => 'select',
    'renderType' => 'selectSingle',
    'items' => [
      ['Par défaut', ''],
      ['Centrer', 'margin:auto'],
      ['A droite', 'float:right'],
      ['Gauche', 'float:left']
    ],
  ],
];
