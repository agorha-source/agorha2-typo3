<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
'Sword.AgorhaNews',
'AgorhaNews',
'Détail article ou actualité',
'EXT:agorha_news/Resources/Public/Icons/news_domain_model_news.svg'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhanews_index'] = 'recursive,select_key,pages';
