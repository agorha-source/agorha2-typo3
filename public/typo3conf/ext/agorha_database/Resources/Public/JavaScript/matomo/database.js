function createInteraction(category, action, label) {
  if (typeof(_mtm) !== "undefined") {
    _mtm.push({
      'event': 'interaction',
      'category': category,
      'action': action,
      'label': label
    });
  }
}
