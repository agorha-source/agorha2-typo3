<?php
defined('TYPO3_MODE') or die;

$boot = function() {

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaDatabase',
        'List',
        [
            'Database' => 'list'
        ]
    );

    unset($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['TYPO3\CMS\Frontend\Page\PageGenerator']['generateMetaTags']['canonical']);
};

$boot();
unset($boot);
