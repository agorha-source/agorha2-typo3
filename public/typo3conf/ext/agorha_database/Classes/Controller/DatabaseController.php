<?php

namespace Sword\AgorhaDatabase\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class DatabaseController extends ActionController
{

  /**
   * /explorer les bases - liste les bases de données
   */
  function listAction()
  {
    $typesString = null;
    $array = [];
    // Informations envoyées via checkbox et bouton "Appliquer les filtres"
    if (isset($_POST['tx_agorhadatabase_list'])) {
      $array = $this->createArrayFromForm($_POST['tx_agorhadatabase_list']);
      // Informations envoyées par les link.action sur le nom des facettes ou la suppression des filtres ajoutées
    } else if (($this->request->getArguments()['deletedType'] && $this->request->getArguments()['types']) || $this->request->getArguments()['setFilters']) {
      if($this->request->getArguments()['deletedType']) {
        $filters = strtolower($this->request->getArguments()['deletedType']);
      } else {
        $filters = strtolower($this->request->getArguments()['setFilters']);
      }
      $array = $this->request->getArguments()['types'];
      // Suppression du filtre s'il existe déjà
      if($array[$filters]) {
        $array[$filters] = '';
      } else  {
        // Sinon ajout du filtre
        $array[$filters] =  $this->request->getArguments()['setFilters'];
      }
    }
    $types = $this->createArrayWithNonEmptyValues($array);
    // Création d'une string contenant les filtres - utile pour la requête back
    $typesString = $this->getTypesNeeded($types);

    // Récupération databases and facets
    $data = $this->getData($typesString);

    $this->view->assignMultiple([
      'data' => $data,
      'types' => $array
    ]);
  }

  /**
   * Construit la requête sur le filtre des types de bases
   * selon ce que l'utilisateur a coché
   *
   * @param $types
   * @return string
   */
  private function getTypesNeeded($types)
  {
    $string = '';
    foreach ($types as $key => $type) {
      if($type === 'Non renseignée') {$type = 'UNKNOWN';}
      if ($key == 0) {
        $string = '?type=' . $type;
      } else {
        $string .= '&type=' . $type;
      }
    }
    return $string;
  }

  /**
   * Permet de ne récupérer que les valeurs nécessaires envoyées par $_POST
   *
   * @param $values
   * @return mixed
   */
  private function createArrayFromForm($values)
  {
    $types['artwork'] = $values['artwork'];
    $types['person'] = $values['person'];
    $types['reference'] = $values['reference'];
    $types['event'] = $values['event'];
    $types['collection'] = $values['collection'];
    $types['non renseignée'] = $values['non renseignée'];
    return $types;
  }

  /**
   * Créé un tableau 0 => valeur au lieu de clé => valeur
   * Ne push pas la valeur si elle est vide
   * Pour ensuite créer la bonne requête (?filtre=valeur1&filtre=...)
   *
   * @param $types
   * @return array
   */
  private function createArrayWithNonEmptyValues($types)
  {
    $array = [];
    foreach ($types as $key => $value) {
      if ($value) {
        array_push($array, $value);
      }
    }
    return $array;
  }

  /**
   * Retourne la liste des bases de données avec ou sans filtres et les facettes
   *
   * @param null $types
   * @return mixed|null
   */
  private function getData($types = null)
  {
    $url = $GLOBALS['AGORHA2_API_URL'] . '/database/list' . $types;
    $client = new Client();
    try {
      $response = $client->get($url);
      return \GuzzleHttp\json_decode($response->getBody());
    } catch (RequestException $e) {
      return null;
    }
  }


}
