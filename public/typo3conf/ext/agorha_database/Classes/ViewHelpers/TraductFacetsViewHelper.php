<?php

namespace Sword\AgorhaDatabase\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class TraductFacetsViewHelper extends AbstractViewHelper{

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('type', 'string', 'Name of the current type to translate');
  }

  /**
   * Traduit le type de la bdd en français
   *
   * @return string
   */
  public function render() {
    switch ($this->arguments['type']) {
      case 'ARTWORK': {
        $research = 'Oeuvre';
        break;
      }
      case 'PERSON': {
        $research = 'Personne';
        break;
      }
      case 'REFERENCE': {
        $research = 'Reference';
        break;
      }
      case 'EVENT': {
        $research = 'Evènement';
        break;
      }
      case 'COLLECTION': {
        $research = 'Collection';
        break;
      }
      default:
        return $this->arguments['type'];
    }
    return 'Bases de type '. $research;
  }
}
