<?php

namespace Sword\AgorhaDatabase\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class StatusViewHelper extends AbstractViewHelper{

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('status', 'number', 'Statut of Database');
  }

  /**
   * Combines two arrays using one for keys and
   * the other for values. If values are not provided
   * in argument it can be provided as tag content.
   *
   * @return string
   */
  public function render() {
    switch ($this->arguments['status']){
      case '1':
        $status = 'Non publiée';
        break;
      case '2':
        $status = 'En cours de réalisation';
        break;
      case '3':
        $status = 'Finalisée';
        break;
      case '4':
        $status = 'Mise à jour fréquente';
        break;
    }
    return $status;
  }
}
