<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaDatabase',
    'List',
    'Liste des bases de données',
    'EXT:agorha_base/Resources/Public/Icons/COLLECTION.svg'
);


$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhadatabase_list'] = 'recursive,select_key,pages';

// Ajout du flexform sur agorhanuxtjs_listdatabases
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['agorhadatabase_list'] = 'pi_flexform';

/***************
 * Add flexForms for plugin configuration
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'agorhadatabase_list',
    'FILE:EXT:agorha_database/Configuration/FlexForms/flexform_database_page.xml'
);
