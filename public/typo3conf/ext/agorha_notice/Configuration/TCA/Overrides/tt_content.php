<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Sword.AgorhaNotice',
    'Modif',
    'Modification d\'une notice'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
  'Sword.AgorhaNotice',
  'ListSelection',
  'Liste les sélections de notices',
  'EXT:agorha_base/Resources/Public/Icons/collections_bookmark.svg'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
  'Sword.AgorhaNotice',
  'CreateNotices',
  'Création de notices',
  'EXT:agorha_base/Resources/Public/Icons/collections_bookmark.svg'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhanotice_modif'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhanotice_listselection'] = 'recursive,select_key,pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['agorhanotice_createnotices'] = 'recursive,select_key,pages';
