<?php

namespace Sword\AgorhaNotice\Controller;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;

/**
 * ContributionController
 */
class ContributionController extends UtilityController
{

  public function __construct()
  {
    parent::__construct();
    $this->deleteMetaTag();
  }

  /**
   * /contribution
   * action createNotice
   * @return void
   * @throws GuzzleException
   */
  public function indexAction() {
    if ($this->isAuthenticated()) {
      try {
        $url = $this->getContributionUrl();
        $response = $this->guzzleRequestToString($url, $this->getCookieJar());
        $data = $response ? $this->splitTitleAndBodyFromPage($response) : false;
        if($data) {
          $this->view->assign('nuxt_data', $data);
        } else {
          $this->redirectToPage($this->uidPage['500'],302);
        }
      }
      catch (RequestException $e){
        $this->redirectToPage($this->uidPage['404'],302);
      }
    } else {
      // Page de login
      $this->redirectToPage($this->uidPage['login'], 302);
    }
  }

  /**
   * @return string
   */
  private function getContributionUrl(){
    return $searchURI = $this->getNuxtUrl(). 'contribution';
  }

}
