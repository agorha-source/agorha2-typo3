<?php

namespace Sword\AgorhaNotice\Controller;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * SelectionController
 */
class SelectionController extends UtilityController
{
    const MAX_ITEMS = 10000;
    /**
     * /mes-selections
     * action list
     */
    public function listAction()
    {
        // Récupération du nombre de sélections souhaité
        $selectionsNumberPerPage = 10;
        if (GeneralUtility::_GP('itemsPerPage')) {
            $selectionsNumberPerPage = GeneralUtility::_GP('itemsPerPage');
        }

        // Est-ce que l'utilisateur est connecté ?
        if ($this->isAuthenticated()) {
            // Est-ce que l'utilisateur a utilisé le filtre ?
            if (isset($_POST['tx_agorhanotice_listselection']['noticeFilter'])) {
                // Requête avec termes recherchés
                $data = $this->getUserNoticesBaskets($_POST['tx_agorhanotice_listselection']['noticeFilter']);
            } else {
                // Requête simple
                $data = $this->getUserNoticesBaskets();
            }
            $this->view->assignMultiple([
                'userNoticesBaskets' => $data,
                'agorhaApiUrl' => $this->getApiUrl(),
                'searchUrl' => $this->getSearchUrl(),
                'selectionsNumberPerPage' => $selectionsNumberPerPage
            ]);
        } else {
            // Si ce n'est pas le cas : redirection vers la page Login (uid: 28)
            $this->redirectToPage($this->uidPage['login'], 302);
        }
    }

    /**
     * Récupère les sélections de notices liées à l'utilisateur connecté
     * @param null $term
     * @return bool|mixed
     * @throws GuzzleException
     */
    private function getUserNoticesBaskets($term = null) {
        try {
            $url = $this->getApiUrl() . '/users/selections?terms='. $term .'&page=1&pageSize=10000';
            $response = $this->client->request('GET', $url, ['cookies' => $this->getCookieJar()]);
        } catch (RequestException $e){}
        if(isset($response) && ($response->getStatusCode() === 200)){
            return \GuzzleHttp\json_decode($response->getBody());
        } else {
            return false;
        }
    }

}
