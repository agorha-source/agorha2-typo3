<?php


namespace Sword\AgorhaNotice\Controller;

use GuzzleHttp\Exception\RequestException;
use Sword\AgorhaBase\Controller\UtilityController;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ModificationController extends UtilityController
{

    public function __construct()
    {
        parent::__construct();
        $this->deleteMetaTag();
    }

    /**
     *  /modification
     */
    public function indexAction() {
        // Authentification obligatoire
        if ($this->isAuthenticated()) {
            try {
                $response = $this->guzzleRequestToString($this->getUrl(), $this->getCookieJar());
                if(isset($response)) {
                    $data = $this->splitTitleAndBodyFromPage($response);
                    if($data) {
                        $this->view->assign('nuxt_data', $data);
                    } else {
                        $this->redirectToPage($this->uidPage['500'],302);
                    }
                }
            }
            catch (RequestException $e){
                $this->redirectToPage($this->uidPage['500'],302);
            }
        } else {
            // Redirection homepage si non connecté
            $this->redirectToPage($this->uidPage['homepage'],302);
        }
    }


    /**
     * Permet de retourner l'url de requête
     * @return string
     */
    private function getUrl()
    {
        $database = GeneralUtility::_GP('database') ? '&database=' . GeneralUtility::_GP('database') : '';
        return $this->getNuxtUrl() . 'modification?notice=' . GeneralUtility::_GP('notice') . $database;
    }
}
