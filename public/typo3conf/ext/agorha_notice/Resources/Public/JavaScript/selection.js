/**
 * Templates/Selection/List.html
 *
 * Fonction de suppression de la selection de notice
 *
 * @param selectionId - identifiant de la sélection
 * @param apiUrl - url de l'api
 */
function deleteSelectionNotice(selectionId, apiUrl) {
  $('.modal').modal('show');
  let url = apiUrl + "/users/selections/" + selectionId;
  $('#modal_valid_delete').click(function() {
    $.ajax({
      url: url ,
      method: "DELETE",
      xhrFields: {
        withCredentials: true
      },
      crossDomain: true,
      success: function () {},
      error: function () {},
      complete: function (){
        window.location.replace("/mes-selections");
      },
    })
  })
}

/**
 * Templates/Selection/List.html
 *
 * Fonction de partage de notice
 *
 * @param noticeId - identifiant de la notice
 * @param apiUrl - url de l'api
 */
function shareNotice(noticeId, apiUrl) {
  let dummy = document.createElement("input");
  document.body.appendChild(dummy);
  dummy.setAttribute("id", "dummy_id");
  let getUrl = window.location;
  document.getElementById("dummy_id").value = getUrl.protocol + "//" + getUrl.host + apiUrl + "&selectionId=" + noticeId + "&facets=W10%3D";
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);

  let toast = $('.toast');
  toast.toast({
    delay: 5000,
    autohide: true,
    variant: "success"
  });
  toast.toast('show');
}
