<?php
defined('TYPO3_MODE') or die();

$boot = function () {

    /* ===========================================================================
    Déclaration des plugins AgornaNotice
      =========================================================================== */

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Sword.AgorhaNotice',
        'Modif',
        [
            'Modification' => 'index'
        ],
        [
            'Modification' => 'index'
        ]
    );

  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Sword.AgorhaNotice',
    'ListSelection',
    [
      'Selection' => 'list'
    ],
    [
      'Selection' => 'list'
    ]
  );

  \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Sword.AgorhaNotice',
    'CreateNotices',
    [
      'Contribution' => 'index'
    ],
    [
      'Contribution' => 'index'
    ]
  );

};

$boot();
unset($boot);
