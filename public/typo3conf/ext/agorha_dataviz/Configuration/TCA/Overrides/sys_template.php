<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'agorha_dataviz',
     'Configuration/TypoScript',
      'Composant de datavisualisation'
);
