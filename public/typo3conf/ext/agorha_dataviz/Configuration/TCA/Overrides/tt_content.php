<?php
defined('TYPO3_MODE') or die();

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Composants de datavisualisation',
        'agorhadataviz_dataviz',
        'agorha-dataviz-graph',
    ],
    'mask_urls_list',
    'after'
);

/***************
 * Add flexForms for content element configuration
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    // Flexform configuration schema file
    'FILE:EXT:agorha_dataviz/Configuration/FlexForms/flexform_dataviz.xml',
    'agorhadataviz_dataviz'
);

// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['agorhadataviz_dataviz'] = [
    'showitem' => '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;;general,
        --div--;Option du graphique,
            pi_flexform;LLL:EXT:bootstrap_package/Resources/Private/Language/Backend.xlf:advanced,
    '
];
