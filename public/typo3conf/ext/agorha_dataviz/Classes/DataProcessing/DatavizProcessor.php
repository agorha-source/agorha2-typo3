<?php
declare(strict_types = 1);

namespace Sword\AgorhaDataviz\DataProcessing;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\RequestException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class for data processing for the content element "My new content element"
 */
class DatavizProcessor implements DataProcessorInterface
{

    /**
     * Process data for the content element "My new content element"
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $settings = [];
        // Si c'est un tableau , il n'est pas au bon format
        if(is_array($processedData['data']['pi_flexform'])) {
            $keys = array_keys($processedData['data']['pi_flexform']['data']['sDEF']['lDEF']);
            $values = array_values($processedData['data']['pi_flexform']['data']['sDEF']['lDEF']);
            for($i=0;$i<count($values);$i++) {
                $settings[$keys[$i]] = $values[$i]['vDEF'];
            }
        } else {
            // Permet de récupérer la valeur $processedData['data']['pi_flexform'] qui est une string et de le convertir en un tableau utilisable
            $flexformService = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Service\FlexFormService');
            $settings = $flexformService->convertFlexFormContentToArray($processedData['data']['pi_flexform']);
        }
        $processedData['settings'] = $settings;
        if($settings) {
            $graphType = $settings['graphType'];
            if($graphType === "Bar" || $graphType === "Pie"){
                $url = $this->getNoticeSearchUrl($settings['noticeSelection']) . '&facets=' . $this->createFacetsQueryBase64($settings);
            } else if($graphType === "Map" || $graphType === "Timeline"){
                $fieldsArray = $this->getFieldValues($settings);
                // Tableau contenant tous les jsonPath voulus
                $settings['wantedFields'] = $fieldsArray;
                $fields = $this->createStringFromFieldsArray($fieldsArray);
                if($graphType === "Map") {
                    $settings['geoPointsFields'] = $this->getAllGeoPointsFields();
                    $url = $this->getNoticeSearchUrl($settings['noticeSelection']) . $fields;
                } else {
                    $url = $this->getNoticeTimelineUrl($settings['noticeSelection']) . $fields;
                }
            } else if ($graphType === "NetChart") {
                $url = $GLOBALS['AGORHA2_API_URL'] . '/notice/graph?selectionId=' . $settings['noticeSelection'] . '&noticeId=' . $settings['noticeSource'];
            } else {
                // ScatterPlot
                $datax = $settings['scatterplotDataX'] ? '&fields='.trim($settings['scatterplotDataX']) : '';
                $dataY = $settings['scatterplotDataY'] ? '&fields='.trim($settings['scatterplotDataY']) : '';
                $url = $this->getNoticeSearchUrl($settings['noticeSelection']) . $datax . $dataY;
            }

            $response = $this->createRequest($url);
            $results = $this->returnData($graphType, $response);

            $processedData['results'] = $results;
            $processedData['facetLevel'] = $settings['facetLevel'];
            $processedData['typoSettings'] = json_encode($settings);
        }

        return $processedData;
    }

    /**
     * Créé les facettes en base64 selon les valeurs
     * de facetField, valuesNumber et facetLevel
     *
     * @param $settings
     * @return string
     */
    private function createFacetsQueryBase64($settings) {
        $data["field"] = $settings["facetField"];
        $data["pathHierarchical"] = $settings["facetLevel"] == 0 ? false : true;
        $facets["aggregations"][$settings["title"]] = $data;
        return base64_encode(json_encode($facets));
    }

    /**
     * Retourne tous les champs fields voulus
     *
     * @param $settings
     * @return bool|false|string[]
     */
    private function getFieldValues($settings) {
        $fieldsArray = $this->getAllFieldsInArray($settings);
        return $this->explodeFieldsValues($fieldsArray);
    }

    private function getAllFieldsInArray($settings) {
        $fields['fieldOeuvre'] = $settings['fieldOeuvre'];
        $fields['fieldPersonne'] = $settings['fieldPersonne'];
        $fields['fieldReference'] = $settings['fieldReference'];
        $fields['fieldEvent'] = $settings['fieldEvent'];
        $fields['fieldCollection'] = $settings['fieldCollection'];
        return $fields;
    }

    /**
     * Retourne un tableau contenant les éléments de la string en
     * paramètre ($field) s'il y a plusieurs mots séparés par ';'
     *
     * @param $fieldsArray
     * @return bool|false|string[]
     */
    private function explodeFieldsValues($fieldsArray) {
        $fieldsString = '';
        foreach ($fieldsArray as $field) {
            $fieldsString .= ';'.$field;
        }
        if($fieldsString && !empty($fieldsString)) {
            return explode(';', $fieldsString);
        }else {
            return false;
        }
    }

    /**
     * Retourne l'url notice/search?selectionId={noticeId}
     * @param $noticeSelectionId
     * @return string
     */
    private function getNoticeSearchUrl($noticeSelectionId = null) {
        return $GLOBALS['AGORHA2_API_URL'] . '/notice/search?selectionId='.trim($noticeSelectionId);
    }

    /**
     * Retourne l'url notice/timeline?selectionId={noticeId}
     * @param null $noticeSelectionId
     * @return string
     */
    private function getNoticeTimelineUrl($noticeSelectionId = null) {
        return $GLOBALS['AGORHA2_API_URL'] . '/notice/timeline?terms=&selectionId='.trim($noticeSelectionId);
    }

    /**
     *
     * @param $fieldsArray
     * @return string
     */
    private function createStringFromFieldsArray($fieldsArray) {
        $fields = '';
        for($i=0;$i<count($fieldsArray);$i++) {
            if(!empty($fieldsArray[$i])){
                $fields .= trim('&fields='.$fieldsArray[$i]);
            }
        }
        return $fields;
    }

    /**
     * Créé une requête Guzzle HTTP et renvoie la réponse en format JSON
     *
     * @param $url
     * @return bool|mixed
     */
    private function createRequest($url) {
        try {
            $client = new Client();
            $response = $client->get($url,['cookies' => $this->getCookieJar()]);
        } catch (RequestException $e) {
            return null;
        }
        if(isset($response) && ($response->getStatusCode() === 200)){
            return \GuzzleHttp\json_decode($response->getBody());
        } else {
            return null;
        }
    }

    /**
     * Permet de gérer les données en retour selon le type de graphique voulu
     *
     * @param $graphType
     * @param $response
     * @return false|string
     */
    private function returnData($graphType, $response) {
        switch ($graphType) {
            case "Map":
            case "ScatterPlot" :
                return json_encode($response ? $response->results : '');
            case "Timeline":
                return json_encode($response ? $response->results[0]->data : '');
            case "NetChart" :
                return json_encode($response ? $response : '');
            default:
                return json_encode($response ? $response->facets[0] : '');
        }
    }

    /**
     * Retourne un CookieJar ayant pour valeur le cookie JSESSIONID
     * @return CookieJar
     */
    public function getCookieJar() {
        return CookieJar::fromArray(['JSESSIONID' =>  $_COOKIE['JSESSIONID']], $GLOBALS['COOKIE_DOMAIN']);
    }

    /**
     * Retourne les geoPointsFields - utile pour la Map
     * @return mixed|null
     */
    private function getAllGeoPointsFields()
    {
        try {
            $client = new Client();
            $response = $client->get($this->getApiUrl() . "/notice/geoPointField");
        } catch (RequestException $e) {
            return null;
        }
        if(isset($response) && ($response->getStatusCode() === 200)){
            return \GuzzleHttp\json_decode($response->getBody());
        } else {
            return null;
        }
    }

    /**
     * Retourne la variable globale AGORHA2_API_URL
     * @return mixed
     */
    private function getApiUrl()
    {
        return $GLOBALS['AGORHA2_API_URL'];
    }
}
