<?php
namespace Sword\AgorhaDataviz\Hooks\PageLayoutView;

use TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Contains a preview rendering for the page module of CType="agorhadataviz_dataviz"
 */
class DatavizElementPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * Preprocesses the preview rendering of a content element of type "agorhadataviz_dataviz"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {
        if ($row['CType'] === 'agorhadataviz_dataviz') {
            $element = [];
            $headerContent = "<strong>Composant de datavisualisation</strong>";

            // Si c'est un tableau , il n'est pas au bon format
            if(is_array($row['pi_flexform'])) {
                $keys = array_keys($row['pi_flexform']['data']['sDEF']['lDEF']);
                $values = array_values($row['pi_flexform']['data']['sDEF']['lDEF']);
                for($i=0;$i<count($values);$i++) {
                    $element[$keys[$i]] = $values[$i]['vDEF'];
                }
            } else {
                // Permet de récupérer la valeur $processedData['data']['pi_flexform'] qui est une string et de le convertir en un tableau utilisable
                $flexformService = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Service\FlexFormService');
                $element = $flexformService->convertFlexFormContentToArray($row['pi_flexform']);
            }

            $itemContent .= '<p>Type de graphique : ' . $element['graphType'] . '</p>';
            $itemContent .= '<p>Titre du grapique : ' . $element['title'] . '</p>';
            $itemContent .= '<p>Identifiant sélection notices : ' . $element['noticeSelection'] . '</p>';

            $drawItem = false;
        }
    }
}
