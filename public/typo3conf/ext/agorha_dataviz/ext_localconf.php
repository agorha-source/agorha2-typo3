<?php

// Ajout du fichier tsconfig à l'extension
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:agorha_dataviz/Configuration/TsConfig/Page/ContentElement/Element/Dataviz.tsconfig">'
);

// Enregistrement de l'icone utilisé par le content element
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
// use same identifier as used in TSconfig for icon
$iconRegistry->registerIcon(
    'agorha-dataviz-graph',
    'TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider', array(
        'source' => 'EXT:agorha_dataviz/Resources/Public/Icons/graph.svg'
    )
);

// Backend Preview agorhadataviz_dataviz
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['agorhadataviz_dataviz'] =
    \Sword\AgorhaDataviz\Hooks\PageLayoutView\DatavizElementPreviewRenderer::class;
