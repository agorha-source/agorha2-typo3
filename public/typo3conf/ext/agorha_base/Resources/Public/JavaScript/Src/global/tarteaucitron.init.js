tarteaucitronForceLanguage = 'fr';

var tarteaucitronCustomText = {
    "alertBigPrivacy": "En navigant sur notre site, vous acceptez notre <a href='/cookies'>politique concernant les cookies. </a>",
    "acceptAll": "OK"
};

tarteaucitron.init({
    "privacyUrl": "", /* Privacy policy url */

    "hashtag": "#tarteaucitron", /* Open the panel with this hashtag */
    "cookieName": "tarteaucitron", /* Cookie name */

    "orientation": "bottom", /* Banner position (top - bottom) */
    "showAlertSmall": false, /* Show the small banner on bottom right */
    "cookieslist": true, /* Show the cookie list */
    "showIcon": false,  /* Show cookie icon to manage cookies */
    "adblocker": false, /* Show a Warning if an adblocker is detected */
    "AcceptAllCta" : true, /* Show the accept all button when highPrivacy on */
    "highPrivacy": true, /* Disable auto consent */
    "handleBrowserDNTRequest": false, /* If Do Not Track == 1, disallow all */

    "removeCredit": false, /* Remove credit link */
    "moreInfoLink": true, /* Show more info link */
    "useExternalCss": false, /* If false, the tarteaucitron.css file will be loaded */

    "DenyAllCta" : true  /* Show the deny all button */
});

tarteaucitron.user.matomotmUrl = 'https://statistiques.inha.fr/js/container_1nK1yaFh.js';
(tarteaucitron.job = tarteaucitron.job || []).push('matomotm');
