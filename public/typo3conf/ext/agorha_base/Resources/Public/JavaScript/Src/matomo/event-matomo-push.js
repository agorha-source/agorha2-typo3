/** La variable _mtm est déclarée à l'avance pour qu'elle soit disponible rapidement côté Nuxt.
 * Elle est initialement déclarée par tarteaucitron.services.js, appelé par tarteaucitron,
 * mais il est important qu'elle soit disponible avant.
*/
var _mtm = window._mtm = window._mtm || [];

/**
 * Fonction de tracking utilisateur - appelé sur chaque page
 * @param userAuth
 * @param language
 */
function matomoUserTracking(userAuth, language) {
    (language === "0") ? language = "FR" : language = "EN";
    if (userAuth) {
        dataLayerPush({
            'event': 'user',
            'user': {
                'userLoginStatus': "logged",
                'userLanguage': language
            }
        })
    } else {
        dataLayerPush({
            'event': 'user',
            'user': {
                'userLoginStatus': "unlogged",
                'userLanguage': language
            }
        })
    }
}

function dataLayerPush(data) {
    if(typeof(_mtm) !== "undefined") {
        _mtm.push(data);
    }
}

/**
 *
 * Matomo tracking - Affichage des pages
 *
 * @param page404 - Y or N
 * @param pageType - Type de page consultée
 * @param siteSection - search, notices, contribution, édito
 * @param searchKeywords - section search et édito
 * @param searchCategory - section search et édito
 * @param searchResultsNumber - section search et édito
 * @param recordName - section notice
 * @param recordType - section notice
 * @param recordRelatedProgram - section notice
 * @param editoContentType - section édito
 */
function matomoPageTracking(
    page404,
    pageType,
    siteSection,
    searchKeywords = null,
    searchCategory = null,
    searchResultsNumber = null,
    recordName = null,
    recordType = null,
    recordRelatedProgram = null,
    editoContentType = null
) {
    let data = ({
        'event': 'page',
        'page': {
            'page404': page404,
            'pageType': pageType,
            'siteSection': siteSection,
            'searchKeywords' : searchKeywords,
            'searchCategory' : searchCategory,
            'searchResultsNumber': searchResultsNumber,
            'recordName': recordName,
            'recordType': recordType,
            'recordRelatedProgram': recordRelatedProgram,
            'editoContentType': editoContentType
        }
    });

    // Supprime les propriétées nulles
    for (let propName in data.page) {
        if (data.page[propName] === null || data.page[propName] === undefined) {
            delete data.page[propName];
        }
    }

    dataLayerPush(data);
}

function createInteraction(category, action, label) {
    dataLayerPush({
        'event': 'interaction',
        'category': category,
        'action': action,
        'label': label
    })
}
