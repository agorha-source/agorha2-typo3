module.exports = function(grunt) {

    /**
     * Project configuration.
     */
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        paths: {
            root: '../../',
            resources: '<%= paths.root %>Resources/',
            fonts: '<%= paths.resources %>Public/Fonts/',
            img: '<%= paths.resources %>Public/Images/',
            icons: '<%= paths.resources %>Public/Icons/',
            cssicons: '<%= paths.resources %>Public/Icons/GruntIcons/',
            js: '<%= paths.resources %>Public/JavaScript/'
        },
        banner: '/*!\n' +
            ' * Global scripts for this website v<%= pkg.version %> (<%= pkg.homepage %>)\n' +
            ' */\n',
        uglify: {
            all: {
                options: {
                    banner: '<%= banner %>',
                    mangle: true,
                    compress: true,
                    beautify: false
                },
                files: {
                    '<%= paths.js %>/Dist/global.min.js': [
                        '<%= paths.js %>Src/global/**/*.js'
                    ]
                }
            }
        },
        imagemin: {
            extension: {
                files: [{
                    expand: true,
                    cwd: '<%= paths.resources %>',
                    src: [
                        '**/*.{png,jpg,gif,svg}'
                    ],
                    dest: '<%= paths.resources %>'
                }]
            }
        },
        grunticon: {
            myIcons: {
                files: [{
                    expand: true,
                    cwd: '<%= paths.icons %>',
                    src: ['*.svg', '*.png'],
                    dest: '<%= paths.cssicons %>'
                }],
                options: {
                    enhanceSVG: true,
                    loadersnippet: "grunticon.loader.js",
                }
            }
        },
        watch: {
            options: {
                livereload: true
            },
            javascript: {
                files: '<%= paths.js %>Src/**/*.js',
                tasks: ['js']
            },
            svgicons: {
                files: '<%= paths.icons %>*svg',
                tasks: ['icons']
            }
        }
    });

    /**
     * Register tasks
     */
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-grunticon');

    /**
     * Grunt update task
     */
    grunt.registerTask('js', ['uglify']);
    grunt.registerTask('icons', ['grunticon']);
    grunt.registerTask('build', ['js', 'imagemin']);
    grunt.registerTask('default', ['build']);

};
