import Vue from 'vue';
import App from './App.vue';
import router from './router';

// plugins
import Timeline from 'vue-timelinejs3';
import { LMap, LTileLayer, LMarker, LTooltip, LCircleMarker, LPopup } from 'vue2-leaflet';
import Vue2LeafletMarkercluster from 'vue2-leaflet-markercluster';

// Vue head
import VueHead from 'vue-head';

// widget setup
// (optional) 'Custom elements polyfill'
import 'document-register-element/build/document-register-element';
// vue-custom-element by karol-f
import vueCustomElement from 'vue-custom-element';
Vue.use(vueCustomElement);
Vue.use(Timeline);
Vue.use(VueHead);
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-tooltip', LTooltip);
Vue.component('l-popup', LPopup);
Vue.component('l-circle-marker', LCircleMarker);
Vue.component('l-marker-cluster', Vue2LeafletMarkercluster);

// Style
// require("https://cdn.knightlab.com/libs/timeline3/latest/css/timeline.css");

Vue.config.productionTip = false;

// use vue-custom-element
App.router = router;
Vue.customElement('data-viz', App);
