export default {
    data() {
        return {
            facetsWithAllSubEntries: [],
            arrayOfFacetsValues: []
        }
    },
    methods: {
        // Iteration permet de suivre la données à récupérer une fois splittée
        setFacetsWithAllSubEntries(facet, iteration) {
            // Si la facette a une sous entrée
            if (facet.subEntries) {
                // On sauvegarde la facette
                this.saveFacet(facet, iteration);
                // Puis on itère sur la facette en sous entrée
                this.setFacetsWithAllSubEntries(facet.subEntries, iteration+1);
            } else if (facet[0]) {
                // Il peut y avoir plusieurs sous facettes donc on boucle sur les possibles subEntries présentes
                for (let j = 0; j < facet.length; j ++) {
                    this.setFacetsWithAllSubEntries(facet[j], iteration);
                }
            } else {
               this.saveFacet(facet, iteration);
            }
        },
        saveFacet(facet, iteration) {
            /*
                Création d'un tableau qui récupère les différentes dates dans la value
                Ex: "/6e siècle avant notre ère/2e moitié du 6e siècle avant notre ère/4e quart du 6e siècle avant notre ère"
                retourne [0: "", 1: "6e siècle avant notre ère", 2: "2e moitié du 6e siècle avant notre ère", 3: "4e quart du 6e siècle avant notre ère"]
            */
            let splittedFacet = this.splitFacet(facet);
            if (splittedFacet.length === 1) {
                this.arrayOfFacetsValues.push(facet.value);
                this.facetsWithAllSubEntries.push({ "facette": facet.value, "count": facet.count, "level": 1 })
            } else {
                this.arrayOfFacetsValues.push(splittedFacet[iteration]);
                this.facetsWithAllSubEntries.push({ "facette": splittedFacet[iteration], "count": facet.count, "level": iteration })
            }
        },
        /**
         * @private
         */
        splitFacet(facet) {
            return facet.value.split('/');
        }
    }
};
