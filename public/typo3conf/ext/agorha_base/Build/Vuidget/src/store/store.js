import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import {SEARCH_TYPE} from "@/const/_recherche";
import {aggregationsToCheckedNodes, computeAggregations, decode} from "@/helpers/facetHelper";

import axios from 'axios';
import baseApiUrl from "@/config/api-config";
import {loadSchema} from "@/helpers/schemaHelper";

// Query parameters initiaux
const defaultSearchParams = () => {
    return {
        terms: "",
        page: 1,
        pageSize: 20,
        sort: "",
        fieldSort: "",
        noticeType: "",
        type: SEARCH_TYPE.simple,
        selectionId: "",
        facets: "", // (base64)
        /** Front filters : n'est rempli qu'en cas de recherche avancée */
        filters: "", // (base64)
        withAttachment: false
    };
};

// L'état initial des onglets
const defaultTabs = () => {
    return {
        counting: [
            {value: "ALL", count: 0},
            {value: "ARTWORK", count: 0},
            {value: "PERSON", count: 0},
            {value: "EVENT", count: 0},
            {value: "REFERENCE", count: 0},
            {value: "COLLECTION", count: 0},
            {value: "NEWS", count: 0}
        ],
        // L'onglet sélectionné
        selectedTab: "ALL"
    };
};

const defaultAutocompleteResults = () => {
    return {
        ARTWORK: [],
        PERSON: [],
        REFERENCE: [],
        EVENT: [],
        COLLECTION: [],
        NEWS: []
    };
};

export default new Vuex.Store({
    state: {
        // Query parameters
        searchParams: defaultSearchParams(),
        schemas: {
            ARTWORK: {},
            PERSON: {},
            EVENT: {},
            REFERENCE: {},
            COLLECTION: {}
            // NEWS n'a pas de métamodèle
        },
        // L'état des onglets
        tabs: defaultTabs(),
        // L'URL de la recherche qui vient d'être effectuée
        lastSearchUrl: "",
        // Cases cochées dans les arbres de facettes générales et spécifiques
        checkedNodes: [],
        // Aggregations (pour les facettes)
        aggregations: {},

        /// Recherche avancée
        // - Filtres de recherche avancée
        filters: [],
        // - Cible de la recherche avancée
        contentTarget: "NOTICE",
        // Historique des recherches
        lastSearchTerms: [],
        // Ids des résultats sélectionnés lors d'une recherche
        selectedIds: [],
        // Résultats sélectionnés
        selectedResults: [],
        // Les résultats d'autocomplétion rangés par type de notice
        autocompleteResults: defaultAutocompleteResults()
    },
    mutations: {
        /** Remise à l'état initial du compte des onglets */
        resetTabs(state) {
            state.tabs = defaultTabs();
        },
        /** Remise à l'état initial des paramètres de recherche */
        resetSearchParams(state) {
            state.searchParams = defaultSearchParams();
            state.currentTerms = "";
            state.lastSearchUrl = "";
            state.checkedNodes = [];
            state.aggregations = {};
            state.filters = [];
            state.contentTarget = "NOTICE";
            state.selectedIds = [];
        },
        /** Remise à l'état initial des résultats d'autocomplétion */
        resetAutocompleteResults(state) {
            Object.assign(state.autocompleteResults, defaultAutocompleteResults());
        },
        /** Mise à jour de l'objet d'aggregations (pour les facettes) */
        updateAggregations(state, aggregations) {
            state.aggregations = aggregations;
        },
        /** Mise à jour des termes de recherche */
        updateCurrentTerms(state, terms) {
            state.currentTerms = terms;
        },
        /** Mise à jour des filtres pour la recherche avancée */
        updateFilters(state, filters) {
            state.filters = filters;
        },
        /** Mise à jour des filtres pour la recherche avancée */
        updateContentTarget(state, contentTarget) {
            state.contentTarget = contentTarget;
        },
        /** Mise à jour de l'onglet sélectionné */
        updateSelectedTab(state, selectedTab) {
            state.tabs.selectedTab = selectedTab || "ALL";
        },
        /** Mise à jour des paramètres de recherche */
        updateSearchParams(state, searchParams) {
            Object.assign(state.searchParams, searchParams);
        },
        /** Mise à jour de l'historique de recherches */
        updateLastSearchTerms(state, lastSearchTerms) {
            state.lastSearchTerms = lastSearchTerms;
        },
        /**
         * Rajoute les filtres aux aggregations d'après les noeuds
         * cochés dans les arbres de facettes.
         */
        updateAggregationsWithCheckedNodes(state, checkedNodes) {
            for (const node of checkedNodes) {
                const {facetKey, key} = node;
                if(state.aggregations && state.aggregations[facetKey] && state.aggregations[facetKey].filters) {
                    const filters = state.aggregations[facetKey].filters;
                    if (filters && !filters.includes(key)) filters.push(key);
                }
            }
        },
        updateSchemas(state, schema) {
            if (schema) { Object.assign(state.schemas, schema); }
        },
        /** Mise à jour des noeuds déjà cochés */
        setCheckedNodes(state, nodes) {
            state.checkedNodes = nodes;
        },
        /** La liste des résultats est restreinte à sélection passée en paramètres */
        setSelectedResults(state, results) {
            state.selectedResults = results;
        },
        /**
         * Ajout d'un terme à l'historique des recherches.
         * On ne garde que les 10 dernières recherches.
         */
        addLastSearchTerms(state, value) {
            if (value) {
                let items = state.lastSearchTerms;
                items = items.filter(item => item !== value);
                items.unshift(value);
                state.lastSearchTerms = items.slice(0, 10); // 10 maximum
            }
        },
        /** Ajoute un résultat d'autocomplétion au state, selon son type de notice */
        pushToAutocompleteResults(state, result) {
            const noticeType = result.digest.noticeType;
            if (noticeType) {
                state.autocompleteResults[noticeType].push(result);
            }
        }
    },
    actions: {
        async loadSchema({ commit, state, data }) {
            // let schema = await axios.get(baseApiUrl + `metamodeles?noticeType=ARTWORK`).then(response => response.data);
            // this.updateSchemas({ ARTWORK: schema });
            //
            // /** Promesse de schéma en fonction du type de notice */
            // const getSchema = noticeType => axios.get(baseApiUrl + `metamodeles?noticeType=${noticeType}`).then(response => response.data);
            // const schemaPromises = [];
            // const schemaKeys = Object.keys(state.schemas);
            // schemaKeys.map((key) => {
            //     // Si le schéma est vide dans le state, on le charge
            //     if (Object.keys(state.schemas[key]).length === 0) {
            //         schemaPromises.push(getSchema(key));
            //     }
            // });
            // const schemas = await Promise.all(schemaPromises);
            // const [a, p, e, r, c] = schemas;
            // commit("updateSchemas", { ARTWORK: a });
            // commit("updateSchemas", { PERSON: p });
            // commit("updateSchemas", { EVENT: e });
            // commit("updateSchemas", { REFERENCE: r });
            // commit("updateSchemas", { COLLECTION: c });
        },
        /** Autocomplétion */
        async autocomplete(context, query) {
            const urlParams = {
                terms: query.terms,
                noticePageSize: 5,
                lang: "fre",
                noticeType: query.noticeType
            };
            const queryString = new URLSearchParams(urlParams);

            const response = await axios.get(baseApiUrl + `notice/autocomplete?${queryString}`, { progress: false });
            const autocompleteResponse = response.data;
            context.commit("resetAutocompleteResults");
            for (const i in autocompleteResponse) {
                context.commit("pushToAutocompleteResults", autocompleteResponse[i]);
            }
        },
        /**
         * @private
         * Préparation de la requête de recherche :
         * - Si le paramètre filters est rempli, prépare une recherche avancée
         * - Sinon, prépare une recherche simple
         * @returns {URLSearchParams} Les paramètres de recherche
         */ async prepareSearchParams(context) {
            const selectedTab = context.state.tabs.selectedTab;

            // On garde les termes de la recherche en cours
            context.commit("updateCurrentTerms", context.state.searchParams.terms);
            const facets = context.state.searchParams.facets;

            // Le métamodèle récupéré pour les facettes doit être celui de l'onglet courant (selectedTab)
            // Pourquoi pas le filtre de notice (noticeType) ?
            // Car au clic sur un onglet, on souhaite avoir les facettes spécifiques associées
            // Ce qui ne serait pas le cas en utilisant noticeType !
            let schema = {};
            // NEWS n'a pas de métamodèle, on prend ARTWORK à la place
            if (selectedTab === "NEWS") {
                schema = context.rootGetters.getSchemaByKey("ARTWORK");
            } else {
                schema = context.rootGetters.getSchemaByKey(selectedTab);
            }
            if (!schema.properties) {
                schema = await loadSchema(selectedTab === "ALL" ? "ARTWORK" : selectedTab);
                context.commit("updateSchemas", {selectedTab: schema})
            }

            // Récupération des facettes générales (et spécifiques, si onglet sélectionné)
            // dans le métamodèle du type courant
            const specific = selectedTab !== "ALL";
            const emptyAggregations = computeAggregations(schema, specific, selectedTab);

            // Ajout du filtre sur l'onglet
            if (specific) {
                emptyAggregations.noticeType.filters.push(selectedTab);
            }

            context.commit("updateAggregations", emptyAggregations);

            // Extraction des facettes depuis l'URL
            if (facets) {
                const significantAggs = decode(facets);
                const nodesFromURL = aggregationsToCheckedNodes(significantAggs);
                context.commit("setCheckedNodes", nodesFromURL);
                // Ajout des noeuds cochés de l'arbre des facettes aux aggregations
                context.commit("updateAggregationsWithCheckedNodes", nodesFromURL);
            }
            // Extraction des filtres (recherche avancée) depuis l'URL
            const encodedFilters = context.state.searchParams.filters;

            if (encodedFilters) {
                const activeFilters = decode(encodedFilters);
                context.commit("updateFilters", activeFilters);
            }
            // Vider une éventuelle sélection en cours
            context.commit("setSelectedResults", []);
        },
        /**
         * @public
         * Unique point d'entrée de la recherche :
         * - Si filters est rempli, appelle une recherche avancée
         * - Sinon, appelle une recherche simple
         */
        async search(context) {
            await context.dispatch("prepareSearchParams");
            const {searchParams} = context.state;
            const urlSearchParams = new URLSearchParams(searchParams);

            let url = `/recherche?${urlSearchParams.toString()}`;
            window.location.replace(url);
        }
    },
    getters: {
        getSchemaByKey: state => (key) => {
            let actualKey = key;
            // Si la clé recherchée n'est pas dans la liste des modèles
            if (!Object.keys(state.schemas).includes(key)) {
                // On choisit ARTWORK comme schéma par défaut
                actualKey = "ARTWORK";
                console.warn(`Métamodèle '${key}' introuvable, métamodèle '${actualKey}' renvoyé par défaut`);
            }
            return state.schemas;
        },
        // Paramètres de recherche
        getSearchParams: state => state.searchParams,
        // Filtres de recherche avancée
        getFilters: state => state.filters,
        getContentTarget: state => state.contentTarget,
        // Onglets
        getSelectedTab: state => state.tabs.selectedTab,
        // Historique des termes de recherche
        getLastSearchTerms: state => state.lastSearchTerms,
        // Autocomplétion
        getAutocompleteResults: state => state.autocompleteResults,
    }
});
