export function createInteraction(category, action, label, value) {
    let data = label ? {
        event: "interaction",
        category,
        action,
        label
    } : {
        event: "interaction",
        category,
        action
    };
    if (value) {
        data = {
            event: "interaction",
            category,
            action,
            label,
            value
        };
    }
    if (typeof (_mtm) !== "undefined") {
        _mtm.push(data);
    }
}
