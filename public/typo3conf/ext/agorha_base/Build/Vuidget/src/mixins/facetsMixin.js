import { store } from '../store/store'
import axios from 'axios'
import baseApiUrl from '../config/api-config'

export default {
  store,
  methods: {
    /**
     * Récupère les facettes existantes depuis le schéma et ajoute les filtres
     * courants pour le prochain search.
     * Si aucun type de notice n'est passé, on ne prend que les facettes générales.
     * @param {string} noticeType le type de notice recherché (ex: ARTWORK). Peut être null
     */
    async getFacetsEncoded(noticeType) {
      let facets = {
        aggregations: {}
      };
      let base64 = "";
      let specifique = true;
      if (!noticeType) {
        if (store.getters.selectedTab) {
          specifique = true;
          noticeType = store.getters.selectedTab;
        } else {
          specifique = false;
          noticeType = "PERSON";
        }
      }
      let urlSchema =
        baseApiUrl + "metamodeles?noticeType=" + noticeType;
      await axios
        .get(urlSchema)
        .then(response => {
          // Récupération des onglets
          let tabFacets = this.initFacet(
            response.data.properties.internal.properties,
            "noticeType",
            "internal"
          );
          facets.aggregations["noticeType"] = tabFacets;
          // Récupération des facettes
          let schema = response.data.properties.content.properties;
          for (let bloc in schema) {
            if (
              schema[bloc].facetScope &&
              schema[bloc].facetScope === "general"
            ) {
              facets.aggregations[
                schema[bloc].facetDisplayName
              ] = this.initFacet(schema[bloc], bloc, "content");
            } else if (
              schema[bloc].facetScope &&
              schema[bloc].facetScope !== "general" &&
              specifique
            ) {
              facets.aggregations[
                schema[bloc].facetDisplayName
              ] = this.initFacet(schema[bloc], bloc, "content");
            } else {
              this.getSubFacets(
                facets.aggregations,
                schema[bloc],
                "content." + bloc,
                specifique
              );
            }
          }
          // Ajout des filtres courants
          for (let facet in facets.aggregations) {
            for (let i in store.getters.resultsFacets) {
              if (facet === store.getters.resultsFacets[i].data.facetKey) {
                facets.aggregations[facet].filters.push(
                  store.getters.resultsFacets[i].data.key
                );
              }
            }
          }
          let buffer = Buffer.from(JSON.stringify(facets));
          base64 = buffer.toString("base64");
        })
        .catch();
      return base64;
    },
    /**
     * Descente dans le schéma pour trouver les facettes qui ne sont pas au 1er niveau
     * @param {object} facetList liste des facettes existantes
     * @param {object} schemaBloc schema du bloc parcouru
     * @param {string} path chemin du bloc (sans les "properties" et "items")
     * @param {boolean} isSpecific true si le type de facette recherché est spécifié, false
     * sinon (ne retourne alors que les facettes générales)
     */
    getSubFacets(facetList, schemaBloc, path, isSpecific) {
      if (schemaBloc) {
        for (let name in schemaBloc) {
          if (
            schemaBloc[name] &&
            schemaBloc[name].facetScope &&
            (schemaBloc[name].facetScope === "general" ||
              (isSpecific && schemaBloc[name].facetScope !== "general"))
          ) {
            let facet = this.initFacet(schemaBloc[name], name, path);
            facetList[schemaBloc[name].facetDisplayName] = facet;
          } else {
            if (schemaBloc[name] && typeof schemaBloc[name] === "object") {
              let newPath = path;
              if (name !== "properties" && name !== "items") {
                newPath += "." + name;
              }
              this.getSubFacets(
                facetList,
                schemaBloc[name],
                newPath,
                isSpecific
              );
            }
          }
        }
      }
    },
    /**
     * Initialise l'objet facette adapté
     * @param {object} schema le schéma du bloc contenant la facette
     * @param {string} blocName le nom du bloc courant
     * @param {string} path chemin du bloc (sans les "properties" et "items")
     */
    initFacet(schema, blocName, path) {
      let pathHierarchical = false;
      if (schema.facetType && schema.facetType === "hierarchical") {
        pathHierarchical = true;
      }
      let facet = {
        filters: [],
        field: path + "." + blocName,
        facetScope: schema.facetScope,
        facetOrder: schema.facetOrder,
        pathHierarchical: pathHierarchical
      };
      return facet;
    },
    /**
     * Décode une chaîne de caractères données en base64
     * @param {string} str la chaîne de caractère encodée en base64
     */
    b64DecodeUnicode(str) {
      // From bytestream, to percent-encoding, to original string.
      return decodeURIComponent(
        atob(str)
          .split("")
          .map(function(c) {
            return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join("")
      );
    }
  }
};
