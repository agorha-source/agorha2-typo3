/** Types d'affichage des résultats de recherche */
export const DISPLAY_TYPE = Object.freeze({
    cartouche: "cartouche",
    mosaique: "mosaique",
    liste: "liste"
});

/** Types de recherche */
export const SEARCH_TYPE = Object.freeze({
    simple: "simple",
    advanced: "advanced"
});

/** Champ sur lequel le tri est appliqué  */
export const FEILD_SORT = Object.freeze({
    TITLE: "TITLE", // associé à l'ordre ASC
    UPDATED_DATE: "UPDATED_DATE" // associé à l'ordre DESC
});

/** Types de tri */
export const SORT_ORDER = Object.freeze({
    ASC: "ASC",
    DESC: "DESC"
});

const pageSizeEnum = {
    "20": "20 résultats par page",
    "50": "50 résultats par page",
    "100": "100 résultats par page"
};

const fieldSortEnum = {
    "": "Tri par pertinence",
    UPDATED_DATE: "Tri par ordre chronologique",
    TITLE: "Tri par ordre alphabétique"
};

export const SELECTION_MESSAGE =
    "Vous devez d’abord sélectionner des résultats dans la recherche pour utiliser cette fonctionnalité.";

// Format du tableau des options de b-form-select
export const pageSizeOptions = Object.keys(pageSizeEnum).map(key =>
    Object({ value: key, text: pageSizeEnum[key] })
);
// Format du tableau des options de b-form-select
export const fieldSortOptions = Object.keys(fieldSortEnum).map(key =>
    Object({ value: key, text: fieldSortEnum[key] })
);
