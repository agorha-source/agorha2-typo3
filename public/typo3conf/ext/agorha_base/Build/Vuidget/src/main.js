import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import i18n from './config/lang-config'
import './assets/scss/main.scss'
// widget setup
// 'Custom elements polyfill'
import 'document-register-element/build/document-register-element'
// vue-custom-element by karol-f
import vueCustomElement from 'vue-custom-element'

Vue.use(vueCustomElement)

Vue.config.productionTip = true

Vue.use(BootstrapVue)

// use vue-custom-element
App.i18n = i18n
App.router = router
App.store = store
Vue.customElement('search-bar', App)


