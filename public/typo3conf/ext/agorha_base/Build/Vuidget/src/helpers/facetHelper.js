/**
 * Construit et retourne l'objet 'aggregations' pour requête au moteur de recherche.
 * Cet objet permet de filtrer les résultats de recherche.
 * @param {Object} schema le métamodèle listant les filtres possibles
 * @param {Boolean} specific true si le type de facette recherché est spécifié, false
 * sinon (ne retourne alors que les facettes générales)
 * @param selectedTab {string} utile pour les facettes des news
 */
export function computeAggregations(schema, specific, selectedTab) {
    const aggregations = {};
    const internalProps = schema.properties.internal.properties;
    const contentProps = schema.properties.content.properties;
    // Facettes des onglets
    aggregations["noticeType"] = initFacet(internalProps.noticeType, "noticeType", "internal");
    // Facettes statiques sur status
    aggregations[internalProps.status.facetDisplayName] = initFacet(internalProps.status, "status", "internal");
    // Récupération des facettes
    if (selectedTab !== "NEWS") {
        for (const bloc in contentProps) {
            if (
                contentProps[bloc] &&
                contentProps[bloc].facetScope &&
                contentProps[bloc].facetScope === "general"
            ) {
                const facet = initFacet(contentProps[bloc], bloc, "content");
                aggregations[contentProps[bloc].facetDisplayName] = facet;
            } else if (
                contentProps[bloc] &&
                contentProps[bloc].facetScope &&
                contentProps[bloc].facetScope !== "general" &&
                specific
            ) {
                const facet = initFacet(contentProps[bloc], bloc, "content");
                aggregations[contentProps[bloc].facetDisplayName] = facet;
            } else {
                childrenFacets(
                    aggregations,
                    contentProps[bloc],
                    "content." + bloc,
                    specific
                );
            }
        }
    } else {
        aggregations["Tags Thématiques"] = initFacet({}, "tags", "content", false);
        aggregations["Bases de données"] = initFacet({}, "databases", "internal.digest", false);
        aggregations["Type"] = initFacet({}, "newsType", "internal.digest", false);
        aggregations["Date de création"] = initFacet({}, "createdDatePath", "internal.digest", true);
    }
    return aggregations;
}

/**
 * Initialise l'objet facette adapté
 * @param {object} schema le schéma du bloc contenant la facette
 * @param {string} blocName le nom du bloc courant
 * @param {string} path chemin du bloc (sans les "properties" et "items")
 * @param {boolean} pathHierarchical permet de renseigner si une facette est hiérarchique - utile pour les facettes des news, construites sans schema
 */
function initFacet(schema, blocName, path, pathHierarchical = false) {
    if ((schema && schema.facetType && schema.facetType === "hierarchical") || pathHierarchical) {
        pathHierarchical = true;
    }
    return {
        filters: [],
        field: path + "." + blocName,
        facetScope: schema.facetScope ? schema.facetScope : "",
        facetOrder: schema.facetOrder ? schema.facetOrder : 0,
        pathHierarchical
    };
}

/**
 * Attention : modifie l'objet en entrée (facetList) !
 * Descente dans le schéma pour trouver les facettes qui ne sont pas au 1er niveau
 * @param {object} list liste des facettes existantes
 * @param {object} blockSchema schema du bloc parcouru
 * @param {string} path chemin du bloc (sans les "properties" et "items")
 * @param {boolean} specific true si le type de facette recherché est spécifié, false
 * sinon (ne retourne alors que les facettes générales)
 */
function childrenFacets(list, blockSchema, path, specific) {
    if (blockSchema) {
        for (const name in blockSchema) {
            if (
                blockSchema[name] &&
                blockSchema[name].facetScope &&
                (blockSchema[name].facetScope === "general" ||
                    (specific && blockSchema[name].facetScope !== "general"))
            ) {
                const facet = initFacet(blockSchema[name], name, path);
                list[blockSchema[name].facetDisplayName] = facet;
            } else if (blockSchema[name] && typeof blockSchema[name] === "object") {
                let newPath = path;
                if (name !== "properties" && name !== "items") {
                    newPath += "." + name;
                }
                childrenFacets(list, blockSchema[name], newPath, specific);
            }
        }
    }
}

/**
 * Sérialise en base64 un objet de facettes
 * ou de filtres (dans le cas d'une recherche avancée)
 * pour manipulation dans les URL.
 *
 * @param {Object} f facets ou filters
 */
export function encode (f) {
    return Buffer.from(JSON.stringify(f)).toString("base64");
}

/**
 * Désérialise une chaîne représentant des facettes
 * ou des filtres (dans le cas d'une recherche avancée)
 * provenant d'une URL.
 *
 * @param {String} text URL encodée + base64
 */
export function decode (text) {
    return JSON.parse(b64DecodeUnicode(decodeURIComponent(text)));
}

/**
 * Décode une chaîne de caractères donnée en base64
 * @param {string} str la chaîne de caractère encodée en base64
 */
function b64DecodeUnicode(str) {
    // From bytestream, to percent-encoding, to original string.
    return decodeURIComponent(
        atob(str)
            .split("")
            .map(function(c) {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
    );
}

/**
 * Transforme un objet aggregations en checkedNodes.
 * (i) Ne modifie pas le store vuex.
 * @param {Object} aggregations
 */
export function aggregationsToCheckedNodes(aggregations) {
    const nodes = [];
    // Fabrication de checkedNodes
    Object.keys(aggregations).forEach(aggKey => {
        // On exclut les filtres sur les onglets
        if (aggKey !== "noticeType") {
            const filters = [...aggregations[aggKey].filters];
            filters.forEach(filter => {
                nodes.push({
                    text: filter,
                    facetKey: aggKey,
                    key: filter,
                    id: `from_url::${aggKey}::${filter}`
                });
            });
        }
    });
    return nodes;
}
