import axios from "axios";
import baseApiUrl from "@/config/api-config";


export async function loadSchema(noticeType) {
    if (noticeType && noticeType !== "NEWS") {
        return await axios.get(baseApiUrl + `metamodeles?noticeType=${noticeType}`).then(response => response.data);
    } else {
        return await axios.get(baseApiUrl + `metamodeles?noticeType=ARTWORK`).then(response => response.data);
    }
}
