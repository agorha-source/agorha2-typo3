export default {
    fre: {
        message: {
            welcome: "Bienvenu sur Agorha 2",
            ARTWORK: "Œuvre",
            PERSON: "Personne",
            EVENT: "Evénement",
            REFERENCE: "Référence",
            COLLECTION: "Collection",
            filtreARTWORK: "Œuvres",
            filtrePERSON: "Personnes",
            filtreEVENT: "Evénements",
            filtreREFERENCE: "Références",
            filtreCOLLECTION: "Collections"
        }
    },
    eng: {
        message: {
            welcome: "Welcome on Agorha 2",
            ARTWORK: "Artwork",
            PERSON: "Person",
            EVENT: "Event",
            REFERENCE: "Reference",
            COLLECTION: "Collection",
            filtreARTWORK: "Artworks",
            filtrePERSON: "Persons",
            filtreEVENT: "Events",
            filtreREFERENCE: "References",
            filtreCOLLECTION: "Collections"
        }
    }
}
