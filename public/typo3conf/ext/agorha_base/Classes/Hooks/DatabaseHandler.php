<?php

namespace Sword\AgorhaBase\Hooks;

use Sword\AgorhaBase\Utility\MysqlQueryUtility;

class DatabaseHandler extends MysqlQueryUtility {

    /**
     *
     * @param array $fieldArray
     * @param string $table
     * @param int $id
     * @param $parentObject \TYPO3\CMS\Core\DataHandling\DataHandler
     */
    public function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, $parentObject)
    {
        // On surveille le composant mask_database_header à la création - modification pour récupérer ou créer l'uniqueKey
        if ($table === "tt_content" && $fieldArray['CType'] === "mask_database_header")
        {
            // On récupère toutes les données du composant présent en base - donc seulement fonctionnel en modification
            $maskDatabaseHeader = $this->getContentElement($id);
            /*
               Si le composant vient d'être créé on aura une valeur dans $fieldArray['pid']
               Pour la modification $fieldArray['pid'] n'existera plus
            */
            $pid = $fieldArray['pid'] ? $fieldArray['pid'] : $maskDatabaseHeader['pid'];
            if ($this->contentElementIsContainedInDatabasePage($pid))
            {
                if ((int)$fieldArray['tx_mask_database_unique_key'] === 0)
                {
                   $lastDatabaseUniqueKey = $this->getLastDatabaseUniqueKey();
                   $fieldArray['tx_mask_database_unique_key'] = $lastDatabaseUniqueKey + 1;
                }
                $fieldArray['tx_mask_database_author'] = $fieldArray['tx_mask_database_author'] ? $fieldArray['tx_mask_database_author'] : $GLOBALS['BE_USER']->user['realName'];
                $this->setDatabasePagePermalink($pid,'/database/'. $fieldArray['tx_mask_database_unique_key']);
            }
        }

    }


}
