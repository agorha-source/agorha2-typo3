<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\Database;

use Sword\AgorhaBase\Utility\DatabasesUtility;
use Sword\AgorhaBase\Utility\FacetsUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class FacetsViewHelper extends AbstractViewHelper {

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('databaseUK', 'number', 'Unique Key of the current database', FALSE);
  }

  /**
   * Retourne les facets en base 64 de la base de données sur laquelle filtrer
   *
   * @return string
   */
  public function render() {
      $facetsUtility = new FacetsUtility();
      $databasesUtility = new DatabasesUtility();
      $facets = [];
      if($this->arguments["databaseUK"]) {
          $database = $databasesUtility->getDatabaseInfo($this->arguments['databaseUK']);
          $facets["Bases de données"] = $facetsUtility->initFacet(
              $database->content->title,
              "content.recordManagementInformation.databaseLabel.value",
              false,
              3,
              "general"
        );
      }
      return $facetsUtility->encodeFacets($facets);
  }

}
