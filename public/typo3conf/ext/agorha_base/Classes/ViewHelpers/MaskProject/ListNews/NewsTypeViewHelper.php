<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\ListNews;

use Sword\AgorhaBase\Utility\FacetsUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Utilisé dans le fichier public/typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Templates/AgorhanewsListNews.html
 * Génère le bon lien de redirection selon le type sélectionné -> Articles / Actualités / Articles & Actualités
 */
class NewsTypeViewHelper extends AbstractViewHelper
{

    /**
     * @return void
     */
    public function initializeArguments()
    {
        // Valeurs possibles 0 - 1 - 2
        $this->registerArgument('type', 'number', 'Type des éléments à récupérer', TRUE);
    }

    /**
     * Retourne le lien vers les news avec facets si nécessaire
     *
     * @return int
     */
    public function render()
    {
        $facetUtility = new FacetsUtility();
        if($this->arguments['type'] === '0' || $this->arguments['type'] === '1') {
            if ($this->arguments['type'] === '0') {
                $facets["Types"] = $facetUtility->initFacet('Articles', "internal.digest.newsType", false, '', '');
            } else if ($this->arguments['type'] === '1') {
                $facets["Types"] = $facetUtility->initFacet('Actualités', "internal.digest.newsType", false, '', '');
            }
            $facets = $facetUtility->encodeFacets($facets);
            $facets = '&facets=' . $facets;
        }

        return '/recherche?terms=&page=1&pageSize=20&sort=&fieldSort=&noticeType=NEWS&type=simple&selectionId=&affichage=cartouche'.$facets.'&filters=';
    }

}
