<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\ListNews;

use GeorgRinger\News\Domain\Repository\NewsRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class SelectedListViewHelper extends AbstractViewHelper {

    /**
     * ViewHelper utilisé dans le fichier public/typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Partials/NewsList.html
     * Permet de ne renvoyer que le nombre de news demandés -> 3 / 6 / 9 / 12 / 15 / 18
     */

    /**
     * @return void
     */
    public function initializeArguments() {
        $this->registerArgument('values', 'object', 'Values to use in array_combine');
        $this->registerArgument('listId', 'string', 'Id des articles à récupérer', TRUE);
        $this->registerArgument('nbrOfNews', 'string', 'Nombre d\'éléments à afficher', TRUE);
    }

    /**
     * Combines two arrays using one for keys and
     * the other for values. If values are not provided
     * in argument it can be provided as tag content.
     *
     * @return array
     */
    public function render()
    {
       $news = $this->getSelectedNews();
       if($news) {
         return array_slice($news, 0, $this->arguments['nbrOfNews']);
       } else {
         return null;
       }
    }

    /**
     * Récupère les données de chaque news
     * et les retourne dans un tableau
     *
     * @return array
     */
    private function getSelectedNews()
    {
        $newsRecords = null;
        /** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var \GeorgRinger\News\Domain\Repository\NewsRepository $newsRepository */
        $newsRepository = $objectManager->get(NewsRepository::class);
        // On récupère les news
        $idList = GeneralUtility::trimExplode(',', $this->arguments['listId'], true);
        foreach ($idList as $id) {
            $news = $newsRepository->findByIdentifier($id);
            if ($news) {
                $newsRecords[] = $news;
            }
        }
        return $newsRecords;
    }

}
