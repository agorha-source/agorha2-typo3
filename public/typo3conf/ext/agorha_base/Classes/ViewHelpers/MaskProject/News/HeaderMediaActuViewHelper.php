<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\News;

use GeorgRinger\News\Domain\Model\NewsInternal;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 *
 */
class HeaderMediaActuViewHelper extends AbstractViewHelper {

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('newsItem', NewsInternal::class, 'item news');
    $this->registerArgument('headerMedia', 'array', 'liste des news');
  }

  /**
   * Récupère l'oginal file
   *
   * @return array
   */
  public function render() {
    $news = $this->arguments['newsItem'];
    $headerMedia = $this->getHeaderMedia($news);
    if ($headerMedia) {
      return $headerMedia->getOriginalFile()->getIdentifier();
    }
    return null;
  }

  private function getHeaderMedia($news) {
    $data = null;
    $mediaFile = $news->getHeaderMedia();
    if($mediaFile && $mediaFile[0]) {
      $data = $mediaFile[0]->getOriginalResource();
    }
    return $data;
  }

}
