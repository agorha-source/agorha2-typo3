<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 *
 */
class TraductDigestNameViewHelper extends AbstractViewHelper {

    /**
     * @return void
     */
    public function initializeArguments() {
        $this->registerArgument('name', 'string', 'Nom');
        $this->registerArgument('firstname', 'string', 'Prénom');
    }

    /**
     * Formate le nom et le prénom en le traduisant dans la langue courant
     * au format: "Nom, Prénom"
     *
     * @return string
     */
    public function render()
    {
        $nameRes = null;
        if ($this->arguments['name'] || $this->arguments['firstname']) {
            if ($this->arguments['name']) {
                $nameRes = $this->arguments['name'];
            }
            if ($this->arguments['name'] && $this->arguments['firstname']) {
                $nameRes .= ", ";
            }
            if ($this->arguments['firstname']) {
                $nameRes .= $this->arguments['firstname'];
            }
        }
        return $nameRes;
    }
}
