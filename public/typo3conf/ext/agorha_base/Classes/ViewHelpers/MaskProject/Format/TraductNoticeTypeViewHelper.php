<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 *
 */
class TraductNoticeTypeViewHelper extends AbstractViewHelper {

    /**
     * @return void
     */
    public function initializeArguments() {
        $this->registerArgument('noticeType', 'string', 'Valeur du type de la notice', TRUE);
    }

    /**
     * Retourne la valeur traduite
     *
     * @return string
     */
    public function render() {
        switch ($this->arguments['noticeType']) {
            case 'COLLECTION':
                return 'Collection';
                break;
            case 'PERSON':
                return 'Personne';
                break;
            case 'EVENT':
                return 'Evènement';
                break;
            case 'REFERENCE':
                return 'Référence';
                break;
            case 'NEWS':
                return 'Article Ou Actualité';
                break;
            default:
                return 'Oeuvre';
                break;
        }
    }
}
