<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\Format;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 *
 */
class NoticeDateViewHelper extends AbstractViewHelper
{

    /**
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('startDate', 'object', 'Date de début');
        $this->registerArgument('endDate', 'object', 'Date de fin');
    }

    /**
     * Formate le nom et le prénom en le traduisant dans la langue courant
     * au format: "Nom, Prénom"
     *
     * @return string
     */
    public function render()
    {
        $start = $this->arguments['startDate'];
        $end = $this->arguments['endDate'];
        $computedStart = null;
        $computedEnd = null;

        if ($start) {
            if ($start->prefix) {
                $computedStart = $start->prefix . " ";
            }
            if($start->earliest && $start->earliestFormat) {
                $computedStart .= $this->getDateFormat($start->earliest, $start->earliestFormat);
            }else if ($start->earliest) {
                $computedStart .= $start->earliest;
            }
            if($start->latest && $start->latestFormat) {
                $computedStart .= $this->getDateFormat($start->latest, $start->latestFormat);
            } else if ($start->latest) {
                $computedStart .= $start->latest;
            }

        }
        if ($end) {
            if ($end->prefix) {
                $computedEnd = $end->prefix . " ";
            }

            if($end->earliest && $end->earliestFormat) {
                $computedEnd .= $this->getDateFormat($end->earliest, $end->earliestFormat);
            }else if ($end->earliest) {
                $computedEnd .= $end->earliest;
            }
            if($end->latest && $end->latestFormat) {
                $computedEnd .= $this->getDateFormat($end->latest, $end->latestFormat);
            }else if ($end->latest) {
                $computedEnd .= $end->latest;
            }

        }

        if ($computedStart === $computedEnd) {
            return $computedStart;
        } else {
            $date = $computedStart;
            if ($computedEnd) {
                $date .= " - " . $computedEnd;
            }
        }
        return $date;
    }

    function getDateFormat($date, $format)
    {
        // Préparation du traitement des valeurs
        $format = strtoupper($format);
        // Formattage de la date
        if ($format) {
            if (!$date) {
                return null;
            } else if (strstr($date,"-")) {
                $positivDate = str_replace("-","", $date);
                return $positivDate . " avant notre ère";
            } else {
                return $date;
            }
        } else {
            return $date;
        }
    }
}
