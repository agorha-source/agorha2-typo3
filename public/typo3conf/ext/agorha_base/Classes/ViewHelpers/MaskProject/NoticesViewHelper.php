<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class NoticesViewHelper extends AbstractViewHelper {

    /**
     * @return void
     */
    public function initializeArguments() {
        $this->registerArgument('values', 'object', 'Values to use in array_combine');
        $this->registerArgument('noticeId', 'string', 'Identifiant de la sélection de notices', TRUE);
        $this->registerArgument('numberOfNotices', 'string', 'Nombre de notices à afficher', TRUE);
    }

    /**
     * Combines two arrays using one for keys and
     * the other for values. If values are not provided
     * in argument it can be provided as tag content.
     *
     * @return array
     */
    public function render() {
        $client = new Client();
        $apiUrl = $GLOBALS['AGORHA2_API_URL'];
        $cookie = CookieJar::fromArray(['JSESSIONID' =>  $_COOKIE['JSESSIONID']], $GLOBALS['COOKIE_DOMAIN']);
        $url = $apiUrl . "/notice/search?page=1&pageSize=20&selectionId=".$this->arguments['noticeId'];
        try {
            $response = $client->get($url, ['cookies' => $cookie]);
            $data =  \GuzzleHttp\json_decode($response->getBody());
            if($data->results) {
                // On récupère les images des notices
                $notices = $this->noticeHasMultipleImages($data->results, "THUMBNAILS");
                // On ne renvoit que le nombre de notices souhaités
                return array_slice($notices, 0, $this->arguments['numberOfNotices']);
            } else {
                return null;
            }
        } catch (ServerException $exception) {
            // Erreur 500 : sélection de notice n'existe plus
            return null;
        } catch (ConnectException $exception) {
            // Url non disponible
            return null;
        }
    }

    /**
     * Récupère la pref picture de la notice si présente puis tous les médias de la notice
     * Merge les deux tableaux pour n'en retourner qu'un
     * @param $notices
     * @param $mode
     * @return mixed
     */
    private function noticeHasMultipleImages($notices, $mode)
    {
        $files = [];
        $cardImgUrl = [];
        for($i = 0; $i < count($notices); $i++) {

            if ($notices[$i]->digest->noticeType != 'NEWS') {
                // Récupération prefPicture présente dans les résultats
                if ($notices[$i]->content->mediaInformation->prefPicture) {
                    $cardImgUrl["mediaType"] = $notices[$i]->content->mediaInformation->prefPicture->mediaType;
                    $cardImgUrl["idFile"] = $notices[$i]->content->mediaInformation->prefPicture->idFile;
                    $cardImgUrl["idMedia"] = $notices[$i]->content->mediaInformation->prefPicture->idMedia;
                    $cardImgUrl["thumbnail"] = $notices[$i]->content->mediaInformation->prefPicture->thumbnail;
                    if ($cardImgUrl) {
                        array_push($files, $cardImgUrl);
                    }
                }

                // Récupération de toutes les images de la notice
                $imgThumbnail = $this->getNoticeMedia($notices[$i]->id, $mode);

                if ($imgThumbnail) {
                    for ($j = 0; $j < count($imgThumbnail); $j++) {
                        for ($k = 0; $k < count($imgThumbnail[$j]->files); $k++) {
                            if ($imgThumbnail[$j]->files[$k]->thumbnail != $cardImgUrl["thumbnail"]) {
                                $notices[$i]->carousel = true;
                                array_push($files, $imgThumbnail[$j]->files[$k]);
                            }
                        }
                    }
                }
            } else {
                if($notices[$i]->digest->headerMedia) {
                    $array["thumbnail"] = $notices[$i]->digest->headerMedia;
                    array_push($files, $array);
                }
            }

            $notices[$i]->files = $files;
            $files = [];
        }
        return $notices;
    }

    /**
     * Récupération des médias d'une notice
     * @param $idNotice
     * @param $mode
     * @return mixed|null
     */
    private function getNoticeMedia($idNotice, $mode) {
        $client = new Client();
        $apiUrl = $GLOBALS['AGORHA2_API_URL'];
        $cookie = CookieJar::fromArray(['JSESSIONID' => $_COOKIE['JSESSIONID']], $GLOBALS['COOKIE_DOMAIN']);
        $url = $apiUrl . "/notice/" . $idNotice . "/media?mode=" . $mode;
        try {
            $response = $client->get($url, ['cookies' => $cookie]);
            return \GuzzleHttp\json_decode($response->getBody());
        } catch (ServerException $exception) {
            return null;
        }
    }
}
