<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\Math;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class DivisionViewHelper extends AbstractViewHelper {

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('nbrOne', 'number', 'Valeur 1');
    $this->registerArgument('nbrTwo', 'number', 'Valeur 2');
  }

  /**
   * Combines two arrays using one for keys and
   * the other for values. If values are not provided
   * in argument it can be provided as tag content.
   *
   * @return number
   */
  public function render() {
    return $this->arguments['nbrOne'] / $this->arguments['nbrTwo'];
  }
}
