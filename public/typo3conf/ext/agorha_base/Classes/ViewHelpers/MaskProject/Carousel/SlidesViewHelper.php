<?php

namespace Sword\AgorhaBase\ViewHelpers\MaskProject\Carousel;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class SlidesViewHelper extends AbstractViewHelper {

  /**
   * @return void
   */
  public function initializeArguments() {
    $this->registerArgument('carouselItems', 'array', 'Values to use in array_combine');
    $this->registerArgument('items', 'array', 'Identifiant de la sélection de notices', TRUE);
    $this->registerArgument('size', 'string', 'Taille de l écran utilisateur', TRUE);
  }

  /**
   * Combines two arrays using one for keys and
   * the other for values. If values are not provided
   * in argument it can be provided as tag content.
   *
   * @return array
   */
  public function render() {
    $carouselItems = array_values($this->arguments['items']);
    $finalArray = [];
    $slideArray = [];
    $cycle = 1;
    $modulo = 4;
    if($this->arguments['size'] && $this->arguments['size'] == 'md'){
      $modulo = 2;
    }
    for($i = 0; $i < count($carouselItems); $i++) {
      if($i == 0) {
        $slideArray['isFirst'] = TRUE;
      }
      array_push($slideArray,$carouselItems[$i]);
      if($cycle % $modulo == 0 || $i === (count($carouselItems)-1)) {
        array_push($finalArray, $slideArray);
        $slideArray = [];
      }
      $cycle += 1;
    }
    return $finalArray;
   }


}
