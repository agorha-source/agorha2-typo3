<?php

namespace Sword\AgorhaBase\Controller;

use DOMDocument;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Sword\AgorhaBase\Utility\FlashUtility;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use Sword\AgorhaBase\Tracker\MatomoTracker;

/**
 * UtilityController
 */
class UtilityController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController implements SingletonInterface
{
    protected $client;
    protected $DOM;
    protected $cookie;
    protected $uidPage;
    protected $matomoTracker;
    protected $serviceMatomo;
    protected $isAuthenticated;

    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
        $this->DOM = new DOMDocument();
        $this->uidPage = $this->getUidConfig();
        $this->serviceMatomo = 'matomotm';

        if (isset($_COOKIE['tarteaucitron'])) {
            $services = array();
            // Décomposition de la chaine de charactère en tableau contenant chaque service et sa valeur
            // on explode() avec '!' et on envlève avec trim() les caractères '!' en début et fin de chaîne
            $rawServices = explode('!', trim($_COOKIE['tarteaucitron'], '!'));
            foreach ($rawServices as $rawService) {
                list($service, $value) = explode('=', $rawService);
                $services[$service] = $value;
            }

            $this->matomoTracker = null;
            if ($services[$this->serviceMatomo] && $services[$this->serviceMatomo] === 'true') {
                // Tarteaucitron existe et il a la valeur matomotm à true. On crée donc une instance du service matomo-php-tracker
                // afin de capter les intéractions spécifiques à la création/suppression/connection d'un compte utilisateur
                $this->matomoTracker = new MatomoTracker($GLOBALS['MATOMO_ID'], $GLOBALS['MATOMO_URL']);
            }
        }
    }

    /**
     * Supprime les balises mise en place par Typo3
     */
    public function deleteMetaTag() {
        $metaTagManager = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);
        $metaTagManager->getManagerForProperty('generator')->removeAllProperties();
        $metaTagManager->getManagerForProperty('twitter:card')->removeAllProperties();
        $metaTagManager->getManagerForProperty('apple-mobile-web-app-capable')->removeAllProperties();
        $metaTagManager->getManagerForProperty('google')->removeAllProperties();
    }

    /**
     * Méthode de redirection
     * @param $targetPageUid : uid de la page voulue
     * @param $statusCode : 200, 302 ...
     * @throws
     */
    public function redirectToPage($targetPageUid, $statusCode) {
        $uri = $this->uriBuilder->setTargetPageUid($targetPageUid)->build();
        $this->redirectToUri($uri, 0, $statusCode);
    }

    /**
     * Créé un message vers le homepageQueue
     * Puis appelle la redirection vers Homepage
     *
     * @param $message
     * @param $header
     * @param $statusCode
     */
    public function redirectToHomepageWithFlashMessage($message, $header, $statusCode) {
        $flashController = new FlashUtility();
        $flashController->createFlashMessageWithQueueIdentifier($message, $header, 0, 'homepageQueue');
        $this->redirectToPage($this->uidPage['homepage'], $statusCode);
    }

    /**
     * @return bool|mixed
     */
    public function isAuthenticated()
    {
        if (!isset($this->isAuthenticated)) {
            try {
                $url = $this->getApiUrl(). '/users/isAuthenticated';
                $response = $this->client->get($url, ['cookies' => $this->getCookieJar()]);
                $this->isAuthenticated = isset($response) ? json_decode($response->getBody()) : false;
            } catch (\Exception $exception) {
                $this->isAuthenticated = false;
            }
        }

        return $this->isAuthenticated;
    }

    /**
     * Requête Guzzle permettant de récupérer le contenu de Nuxt
     * et de le transformer en string pour ensuite le traiter
     * @param $url
     * @param $cookie
     * @return string
     */
    public function guzzleRequestToString($url, $cookie) {
        $response = $this->client->get($url, ['cookies' => $cookie]);
        return $response->getBody()->read($response->getBody()->getSize());
    }

    /**
     * Remet à NULL les infos de l'utilisateur
     */
    public function setSessionToNull() {
        $GLOBALS['TSFE']->fe_user->user = null;
        $GLOBALS['TSFE']->fe_user->setKey('ses','user',null);
        $GLOBALS['TSFE']->fe_user->setKey('ses','role',null);
    }

    /**
     * Retourne la valeur du cookie JSESSIONID
     * @return mixed
     */
    public function getCookie(){
        return $_COOKIE['JSESSIONID'];
    }

    /**
     * Supprime le cookie JSESSIONID de la session
     */
    public function deleteCookie(){
        setcookie('JSESSIONID', null);
    }

    /**
     * Retourne un CookieJar ayant pour valeur le cookie JSESSIONID
     * @return CookieJar
     */
    public function getCookieJar() {
        return CookieJar::fromArray(['JSESSIONID' =>  $this->getCookie()], $this->getDomain());
    }

    /**
     * @return mixed
     */
    public function getDomain(){
        return $GLOBALS['COOKIE_DOMAIN'];
    }
    /**
     * Retourne le nom de l'utilisateur connecté
     * @return string
     */
    public function getUserName(){
        return $GLOBALS['TSFE']->fe_user->getKey('ses','user');
    }

    /**
     * Retourne le rôle de l'utilisateur connecté
     * @return string
     */
    public function getUserRole(){
        return $GLOBALS['TSFE']->fe_user->getKey('ses','role');
    }

    public function isAdmin() {
      return $this->getUserRole() === "ADMIN";
    }

    public function hasRights() {
      return $this->getUserRole() === "ADMIN" || $this->getUserRole() === "DATABASE_MANAGER" || $this->getUserRole() === "CONTRIB";
    }

    /**
     * Retourne l'url de l'api présente dans le fichier AdditionalConfiguration
     * pour pouvoir l'utiliser dans les fluid
     * @return string
     */
    public function getApiUrl() {
        return $GLOBALS['AGORHA2_API_URL'];
    }

    /**
     * @return string
     */
    public function getSearchUrl() {
        return $GLOBALS['RECHERCHE_URL'];
    }

    /**
     * @return mixed
     */
    public function getNuxtUrl() {
        return $GLOBALS['NUXT_URL'];
    }

    /**
     * Retourne le body présent dans la variable $page
     * Injecte les balises head de $page dans le head de Typo3
     * @param $page
     * @return string
     */
    public function splitTitleAndBodyFromPage($page) {
        /* Récupère les balises head de $page (Objet Response) */
        $headBegin = stristr($page, '<head');
        $heads = stristr($headBegin, '</head>', true);

        /* Conversion des data avant chargement DOM */
        $data = mb_convert_encoding($heads, 'HTML-ENTITIES', "UTF-8");
        @$this->DOM->loadHTML($data);

        /* Insertion des différentes balises récupérées dans la partie head de Typo3  */
        $head = $this->DOM->getElementsByTagName("head")->item(0);
        foreach ($head->childNodes as $childNode) {
            $this->response->addAdditionalHeaderData($childNode->ownerDocument->saveHTML($childNode));
        }

        /* Récupération du code entre les balises body de $page */
        $bodyBegin = stristr($page, '<body');
        return stristr($bodyBegin, '</body', true);
    }

    /**
     * Vérifie la force du nouveau mot de passe
     * 8 caractères - au moins 1 caractère numérique
     * au moins 1 caractère spécial , au moins 1 majuscule
     * @param $password
     * @return bool
     */
    public function newPasswordIsStrong($password)
    {
        return preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).{8,}$#', $password) ? true : false;
    }

    /**
     * Récupère le message transmis par la réponse
     *
     * @param $message
     * @return string|string[]
     */
    public function getMessageFromResponse($message) {
        $message = stristr($message, 'response:');
        return str_replace('response:', '', $message);
    }

    /**
     * Récupères les uid des pages Homepage, Login, 404 et 500 dans le fichier
     * UidConfiguration.php
     */
    private function getUidConfig() {
        return include(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('typo3conf/UidConfiguration.php'));
    }
}
