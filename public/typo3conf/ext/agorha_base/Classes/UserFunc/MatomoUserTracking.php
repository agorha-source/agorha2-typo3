<?php

namespace Sword\AgorhaBase\UserFunc;

use Sword\AgorhaBase\Controller\UtilityController;
use TYPO3\CMS\Core\Context\LanguageAspect;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MatomoUserTracking
{
    protected UtilityController $utilityController;
    protected LanguageAspect $languageAspect;

    public function __construct()
    {
        // TODO: passer sur de l'injection de dépendance à partir de TYPO3 10
        $this->utilityController = GeneralUtility::makeInstance(UtilityController::class);
        $this->languageAspect = GeneralUtility::makeInstance(LanguageAspect::class);
    }

    public function getTracker(string $content, array $conf): string
    {
        $isAuthenticated = $this->utilityController->isAuthenticated();
        $pageLanguage = $this->languageAspect->getId();

        return sprintf('<script type="text/javascript">matomoUserTracking("%s", "%s");</script>', $isAuthenticated, $pageLanguage);
    }
}
