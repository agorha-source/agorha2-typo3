<?php

namespace Sword\AgorhaBase\UserFunc;

use Sword\AgorhaBase\Controller\UtilityController;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Page\PageRepository;

class AuthLink
{
    protected UtilityController $utilityController;
    protected TypoScriptFrontendController $tsfe;
    protected PageRepository $pageRepository;

    public function __construct()
    {
        // TODO: passer sur de l'injection de dépendance à partir de TYPO3 10
        $this->utilityController = GeneralUtility::makeInstance(UtilityController::class);
        $this->tsfe = $GLOBALS['TSFE'];
        $this->pageRepository = GeneralUtility::makeInstance(PageRepository::class);
    }

    public function getLink(string $content, array $conf): string
    {
        $isAuthenticated = $this->utilityController->isAuthenticated();

        if (!$isAuthenticated) {
            return '';
        }

        // Si le lien ne doit s'afficher que pour les admins
        if ($conf['admin']) {
            $userRole = $this->utilityController->getUserRole();

            if (!in_array($userRole, ['ADMIN', 'CONTRIB', 'DATABASE_MANAGER'])) {
                return '';
            }
        }

        $typolinkConf = [
            'parameter' => $conf['page'] . '  nav-link'
        ];

        if (!empty($conf['icon'])) {
            $typolinkConf['wrap'] = $conf['icon'];
            $typolinkConf['ATagBeforeWrap'] = 1;
        }

        $page = $this->pageRepository->getPage($conf['page']);
        return '<li>' . $this->tsfe->cObj->typoLink($page['title'], $typolinkConf) . '</li>';
    }
}
