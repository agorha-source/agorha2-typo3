<?php

namespace Sword\AgorhaBase\Tracker;

if (!class_exists('\MatomoTracker')) {
    include_once('MatomoTracker.php');
}

/**
 * Helper function to quickly generate the URL to track a page view.
 *
 * @deprecated
 * @param $idSite
 * @param string $documentTitle
 * @return string
 */
function Piwik_getUrlTrackPageView($idSite, $documentTitle = '')
{
    return Matomo_getUrlTrackPageView($idSite, $documentTitle);
}

/**
 * Helper function to quickly generate the URL to track a goal.
 *
 * @deprecated
 * @param $idSite
 * @param $idGoal
 * @param float $revenue
 * @return string
 */
function Piwik_getUrlTrackGoal($idSite, $idGoal, $revenue = 0.0)
{
    return Matomo_getUrlTrackGoal($idSite, $idGoal, $revenue);
}

/**
 * For BC only
 *
 * @deprecated use MatomoTracker instead
 */
class PiwikTracker extends MatomoTracker {}
