<?php

namespace Sword\AgorhaBase\Utility;

use stdClass;

class FacetsUtility {

    /**
     * Initialise les facettes au format attendu par NuxtJs
     *
     * @param $filters
     * @param $field
     * @param $pathHierarchical
     * @param int $facetOrder
     * @param string $facetScope
     * @return stdClass
     */
    public function initFacet($filters,$field,$pathHierarchical,$facetOrder = 0,$facetScope = "") {
        $facetToEncode = new StdClass();
        $facetToEncode->filters[] = $filters;
        $facetToEncode->field = $field;
        $facetToEncode->pathHierarchical = $pathHierarchical;
        $facetToEncode->facetOrder = $facetOrder;
        $facetToEncode->facetScope = $facetScope;
        return $facetToEncode;
    }

    /**
     * Encode les facettes en base 64
     *
     * @param $facets
     * @return string
     */
    public function encodeFacets($facets) {
        return base64_encode(json_encode($facets));
    }
}
