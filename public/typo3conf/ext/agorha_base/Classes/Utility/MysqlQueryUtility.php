<?php

namespace Sword\AgorhaBase\Utility;


use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MysqlQueryUtility {

    /**
     * Récupère un élément dans la table tt_content dont l'uid est {$uid}
     *
     * @param $uid
     * @return mixed
     */
    protected function getContentElement($uid) {
        $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable('tt_content');
        $statement = $queryBuilder
            ->select('*')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT))
            )
            ->execute();
        return $statement->fetch();
    }


    /**
     * Vérifie si le content element fait partie d'une base de données
     * Un élément est contenu dans une base de données si le pid de l'élément est 50
     *
     * @param $id
     * @return bool
     */
    protected function contentElementIsContainedInDatabasePage($id) {
        $queryBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable("pages");
        // Permet de récupérer les éléments cachés
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        $statement = $queryBuilder
            ->select('*')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->eq('pages.uid', $queryBuilder->createNamedParameter($id, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('pages.pid',50)
            )
            ->execute();
        return $statement->fetch() ? true : false;
    }

    /**
     * Sauvegarde le slug de la page dans le format /database/{uniqueKey}
     * @param $uid
     * @param $pageSlug
     */
    protected function setDatabasePagePermalink($uid,$pageSlug) {
        $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable('pages');
        // Permet de récupérer les éléments cachés
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        $queryBuilder
            ->update('pages')
            ->where(
                $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT))
            )
            ->set('slug', $pageSlug)
            ->execute();
    }


    /**
     * Retourne le dernier uniquekey présent dans la base de données
     *
     * @return mixed
     */
    protected function getLastDatabaseUniqueKey() {
        $queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
            ->getQueryBuilderForTable('tt_content');
        // Permet de récupérer les éléments cachés
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
        $statement = $queryBuilder
            ->select('tx_mask_database_unique_key')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq('tt_content.CType', $queryBuilder->createNamedParameter('mask_database_header', \PDO::PARAM_STR))
            )
            ->orderBy('tx_mask_database_unique_key','DESC')
            ->execute();
        return $statement->fetch()['tx_mask_database_unique_key'];
    }

}
