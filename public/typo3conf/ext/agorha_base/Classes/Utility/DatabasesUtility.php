<?php

namespace Sword\AgorhaBase\Utility;

use GuzzleHttp\Client;

class DatabasesUtility {

    /**
     * Récupère une base de données avec son uniqueKey
     * @param $databaseUniqueKey
     * @return mixed|null
     */
    public function getDatabaseInfo($databaseUniqueKey) {
        $url = $GLOBALS['AGORHA2_API_URL'] . "/database/" . $databaseUniqueKey;
        try {
            $client = new Client();
            $response = $client->get($url);
            return \GuzzleHttp\json_decode($response->getBody());
        } catch (\GuzzleHttp\Exception\ServerException $exception) {
            return null;
        } catch (\GuzzleHttp\Exception\ClientException $clientException) {
            return null;
        }
    }
}
