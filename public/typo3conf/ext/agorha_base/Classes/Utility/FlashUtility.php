<?php
namespace Sword\AgorhaBase\Utility;

/**
 */
class FlashUtility
{

    public function createFlashMessageWithQueueIdentifier($message, $header, $severity, $queue)
    {
        $flashMessage = $this->newFlashMessage($message, $header, $severity);
        $this->addFlashMessageToQueueIdentifier($queue, $flashMessage);
    }

    /**
     * Créé un Flash Message
     *
     * @param $message
     * @param $header
     * @param $severity
     * @return object
     */
    private function newFlashMessage($message, $header, $severity)
    {
        return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessage::class,
            $message,
            $header,
            $severity,
            true
        );
    }

    /**
     * Ajoute le flash message au contexte $queue
     *
     * @param $queue
     * @param $flashMessage
     */
    private function addFlashMessageToQueueIdentifier($queue, $flashMessage)
    {
        $flashMessageService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Messaging\FlashMessageService::class);
        $messageQueue = $flashMessageService->getMessageQueueByIdentifier($queue);
        $messageQueue->addMessage($flashMessage);
    }

}
