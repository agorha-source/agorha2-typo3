<?php

/**
 * Extension Manager/Repository config file for ext "agorha_base".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'agorha base',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99',
            'bootstrap_package' => '10.0.0-10.0.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Sword\\AgorhaBase\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Solène',
    'author_email' => 'solene.murcia@sword-group.com',
    'author_company' => 'Sword',
    'version' => '1.0.0',
];
