<?php
return [
    'BE' => [
        'debug' => true,
        'explicitADmode' => 'explicitAllow',
        'installToolPassword' => '$P$CQZx26air3rPLRn2vVIlKdE188VqUD/',
        'loginSecurityLevel' => 'normal',
    ],
    'DB' => [
        'Connections' => [
            'Default' => [
                'charset' => 'utf8mb4',
                'driver' => 'mysqli',
                'tableoptions' => [
                    'charset' => 'utf8mb4',
                    'collate' => 'utf8mb4_unicode_ci',
                ],
            ],
        ],
    ],
    'EXT' => [
        'extConf' => [
            'backend' => 'a:6:{s:9:"loginLogo";s:0:"";s:19:"loginHighlightColor";s:0:"";s:20:"loginBackgroundImage";s:0:"";s:13:"loginFootnote";s:0:"";s:11:"backendLogo";s:0:"";s:14:"backendFavicon";s:0:"";}',
            'bootstrap_package' => 'a:8:{s:16:"disablePageTsRTE";s:1:"0";s:27:"disablePageTsBackendLayouts";s:1:"0";s:20:"disablePageTsTCEMAIN";s:1:"0";s:20:"disablePageTsTCEFORM";s:1:"0";s:28:"disablePageTsContentElements";s:1:"0";s:20:"disableCssProcessing";s:1:"0";s:24:"disableGoogleFontCaching";s:1:"0";s:17:"disableFontLoader";s:1:"0";}',
            'extensionmanager' => 'a:2:{s:21:"automaticInstallation";s:1:"1";s:11:"offlineMode";s:1:"0";}',
            'gridelements' => 'a:6:{s:20:"additionalStylesheet";s:0:"";s:19:"nestingInListModule";s:1:"0";s:26:"overlayShortcutTranslation";s:1:"0";s:19:"disableDragInWizard";s:1:"0";s:25:"disableCopyFromPageButton";s:1:"0";s:38:"disableAutomaticUnusedColumnCorrection";s:1:"0";}',
            'ig_ldap_sso_auth' => 'a:20:{s:21:"throwExceptionAtLogin";s:1:"1";s:22:"forceLowerCaseUsername";s:1:"0";s:23:"useExtConfConfiguration";s:1:"0";s:26:"enableBELDAPAuthentication";s:1:"1";s:17:"TYPO3BEGroupExist";s:1:"0";s:16:"TYPO3BEUserExist";s:1:"0";s:10:"BEfailsafe";s:1:"1";s:27:"TYPO3BEGroupsNotSynchronize";s:1:"0";s:12:"keepBEGroups";s:1:"0";s:11:"enableBESSO";s:1:"0";s:19:"keepBESSODomainName";s:1:"0";s:26:"enableFELDAPAuthentication";s:1:"0";s:31:"TYPO3FEDeleteUserIfNoLDAPGroups";s:1:"0";s:32:"TYPO3FEDeleteUserIfNoTYPO3Groups";s:1:"0";s:17:"TYPO3FEGroupExist";s:1:"0";s:16:"TYPO3FEUserExist";s:1:"0";s:27:"TYPO3FEGroupsNotSynchronize";s:1:"0";s:12:"keepFEGroups";s:1:"0";s:11:"enableFESSO";s:1:"0";s:19:"keepFESSODomainName";s:1:"0";}',
            'mask' => 'a:9:{s:4:"json";s:59:"typo3conf/ext/mask_project/Resources/Private/Mask/mask.json";s:18:"backendlayout_pids";s:3:"0,1";s:7:"content";s:69:"typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Templates/";s:7:"layouts";s:67:"typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Layouts/";s:8:"partials";s:68:"typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Partials/";s:7:"backend";s:68:"typo3conf/ext/mask_project/Resources/Private/Mask/Backend/Templates/";s:15:"layouts_backend";s:66:"typo3conf/ext/mask_project/Resources/Private/Mask/Backend/Layouts/";s:16:"partials_backend";s:67:"typo3conf/ext/mask_project/Resources/Private/Mask/Backend/Partials/";s:7:"preview";s:49:"typo3conf/ext/mask_project/Resources/Public/Mask/";}',
            'mksearch' => 'a:12:{s:8:"zendPath";s:0:"";s:14:"luceneIndexDir";s:0:"";s:7:"tikaJar";s:0:"";s:14:"tikaLocaleType";s:11:"de_DE.UTF-8";s:25:"postTikaCommandParameters";s:0:"";s:12:"defaultIndex";s:7:"default";s:22:"useInternalElasticaLib";s:1:"1";s:26:"abstractMaxLength_fallback";s:3:"200";s:22:"useCurlAsHttpTransport";s:1:"1";s:22:"enableRnBaseUtilDbHook";s:1:"0";s:25:"secondsToKeepQueueEntries";s:7:"2592000";s:29:"elasticIndexesOpenCloseEnbled";s:1:"1";}',
            'news' => 'a:18:{s:13:"prependAtCopy";s:1:"1";s:6:"tagPid";s:1:"1";s:12:"rteForTeaser";s:1:"0";s:22:"contentElementRelation";s:1:"1";s:21:"contentElementPreview";s:1:"1";s:13:"manualSorting";s:1:"0";s:19:"categoryRestriction";s:0:"";s:34:"categoryBeGroupTceFormsRestriction";s:1:"0";s:19:"dateTimeNotRequired";s:1:"0";s:11:"archiveDate";s:4:"date";s:12:"mediaPreview";s:5:"false";s:20:"advancedMediaPreview";s:1:"1";s:13:"slugBehaviour";s:6:"unique";s:24:"showAdministrationModule";s:1:"1";s:35:"hidePageTreeForAdministrationModule";s:1:"0";s:12:"showImporter";s:1:"0";s:18:"storageUidImporter";s:1:"1";s:22:"resourceFolderImporter";s:12:"/news_import";}',
            'rn_base' => 'a:11:{s:13:"verboseMayday";s:1:"0";s:11:"dieOnMayday";s:1:"1";s:21:"forceException4Mayday";s:1:"1";s:16:"exceptionHandler";s:0:"";s:20:"sendEmailOnException";s:0:"";s:9:"fromEmail";s:0:"";s:24:"send503HeaderOnException";s:1:"1";s:17:"loadHiddenObjects";s:1:"0";s:13:"activateCache";s:1:"0";s:18:"activateSubstCache";s:1:"0";s:8:"debugKey";s:0:"";}',
            'scheduler' => 'a:2:{s:11:"maxLifetime";s:4:"1440";s:15:"showSampleTasks";s:1:"1";}',
        ],
    ],
    'EXTCONF' => [
        'lang' => [
            'availableLanguages' => [
                'fr',
            ],
        ],
    ],
    'EXTENSIONS' => [
        'backend' => [
            'backendFavicon' => '',
            'backendLogo' => '',
            'loginBackgroundImage' => '',
            'loginFootnote' => '',
            'loginHighlightColor' => '',
            'loginLogo' => '',
        ],
        'bootstrap_package' => [
            'disableCssProcessing' => '0',
            'disableFontLoader' => '0',
            'disableGoogleFontCaching' => '0',
            'disablePageTsBackendLayouts' => '0',
            'disablePageTsContentElements' => '0',
            'disablePageTsRTE' => '0',
            'disablePageTsTCEFORM' => '0',
            'disablePageTsTCEMAIN' => '0',
        ],
        'extensionmanager' => [
            'automaticInstallation' => '1',
            'offlineMode' => '0',
        ],
        'gridelements' => [
            'additionalStylesheet' => '',
            'disableAutomaticUnusedColumnCorrection' => '0',
            'disableCopyFromPageButton' => '0',
            'disableDragInWizard' => '0',
            'nestingInListModule' => '0',
            'overlayShortcutTranslation' => '0',
        ],
        'ig_ldap_sso_auth' => [
            'BEfailsafe' => '1',
            'TYPO3BEGroupExist' => '0',
            'TYPO3BEGroupsNotSynchronize' => '0',
            'TYPO3BEUserExist' => '0',
            'TYPO3FEDeleteUserIfNoLDAPGroups' => '0',
            'TYPO3FEDeleteUserIfNoTYPO3Groups' => '0',
            'TYPO3FEGroupExist' => '0',
            'TYPO3FEGroupsNotSynchronize' => '0',
            'TYPO3FEUserExist' => '0',
            'enableBELDAPAuthentication' => '1',
            'enableBESSO' => '0',
            'enableFELDAPAuthentication' => '0',
            'enableFESSO' => '0',
            'forceLowerCaseUsername' => '0',
            'keepBEGroups' => '0',
            'keepBESSODomainName' => '0',
            'keepFEGroups' => '0',
            'keepFESSODomainName' => '0',
            'throwExceptionAtLogin' => '1',
            'useExtConfConfiguration' => '0',
        ],
        'mask' => [
            'backend' => 'typo3conf/ext/mask_project/Resources/Private/Mask/Backend/Templates/',
            'backendlayout_pids' => '0,1',
            'content' => 'typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Templates/',
            'json' => 'typo3conf/ext/mask_project/Resources/Private/Mask/mask.json',
            'layouts' => 'typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Layouts/',
            'layouts_backend' => 'typo3conf/ext/mask_project/Resources/Private/Mask/Backend/Layouts/',
            'partials' => 'typo3conf/ext/mask_project/Resources/Private/Mask/Frontend/Partials/',
            'partials_backend' => 'typo3conf/ext/mask_project/Resources/Private/Mask/Backend/Partials/',
            'preview' => 'typo3conf/ext/mask_project/Resources/Public/Mask/',
        ],
        'mksearch' => [
            'abstractMaxLength_fallback' => '200',
            'defaultIndex' => 'default',
            'elasticIndexesOpenCloseEnbled' => '1',
            'enableRnBaseUtilDbHook' => '0',
            'luceneIndexDir' => '',
            'postTikaCommandParameters' => '',
            'secondsToKeepQueueEntries' => '2592000',
            'tikaJar' => '',
            'tikaLocaleType' => 'de_DE.UTF-8',
            'useCurlAsHttpTransport' => '1',
            'useInternalElasticaLib' => '1',
            'zendPath' => '',
        ],
        'news' => [
            'advancedMediaPreview' => '1',
            'archiveDate' => 'date',
            'categoryBeGroupTceFormsRestriction' => '0',
            'categoryRestriction' => '',
            'contentElementPreview' => '1',
            'contentElementRelation' => '1',
            'dateTimeNotRequired' => '0',
            'hidePageTreeForAdministrationModule' => '0',
            'manualSorting' => '0',
            'mediaPreview' => 'false',
            'prependAtCopy' => '1',
            'resourceFolderImporter' => '/news_import',
            'rteForTeaser' => '0',
            'showAdministrationModule' => '1',
            'showImporter' => '0',
            'slugBehaviour' => 'unique',
            'storageUidImporter' => '1',
            'tagPid' => '1',
        ],
        'rn_base' => [
            'activateCache' => '0',
            'activateSubstCache' => '0',
            'debugKey' => '',
            'dieOnMayday' => '1',
            'exceptionHandler' => '',
            'forceException4Mayday' => '1',
            'fromEmail' => '',
            'loadHiddenObjects' => '0',
            'send503HeaderOnException' => '1',
            'sendEmailOnException' => '',
            'verboseMayday' => '0',
        ],
        'scheduler' => [
            'maxLifetime' => '1440',
            'showSampleTasks' => '1',
        ],
    ],
    'FE' => [
        'debug' => true,
        'passwordHashing' => [
            'className' => 'TYPO3\\CMS\\Core\\Crypto\\PasswordHashing\\Argon2iPasswordHash',
            'options' => [],
        ],
    ],
    'GFX' => [
        'processor' => 'GraphicsMagick',
        'processor_allowTemporaryMasksAsPng' => false,
        'processor_colorspace' => 'RGB',
        'processor_effects' => false,
        'processor_enabled' => true,
        'processor_path' => '/usr/bin/',
        'processor_path_lzw' => '/usr/bin/',
    ],
    'MAIL' => [
        'transport' => 'sendmail',
        'transport_sendmail_command' => '/usr/local/bin/mailhog sendmail test@example.org --smtp-addr 127.0.0.1:1025',
        'transport_smtp_encrypt' => '',
        'transport_smtp_password' => '',
        'transport_smtp_server' => '',
        'transport_smtp_username' => '',
    ],
    'SYS' => [
        'caching' => [
            'cacheConfigurations' => [
                'hash' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend',
                ],
                'imagesizes' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend',
                    'options' => [
                        'compression' => 1,
                    ],
                ],
                'pages' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend',
                    'options' => [
                        'compression' => 1,
                    ],
                ],
                'pagesection' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend',
                    'options' => [
                        'compression' => 1,
                    ],
                ],
                'rootline' => [
                    'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend',
                    'options' => [
                        'compression' => 1,
                    ],
                ],
            ],
        ],
        'devIPmask' => '*',
        'displayErrors' => 1,
        'encryptionKey' => '92dc6c76453405a867997be9071fa2eb778ac63d39f2279fdcc4953b9f38634397b723d98c455993d292bb7499f58b42',
        'exceptionalErrors' => 12290,
        'features' => [
            'security.backend.enforceReferrer' => false,
            'unifiedPageTranslationHandling' => true,
        ],
        'sitename' => 'Agorha',
        'systemLogLevel' => 0,
        'systemMaintainers' => [
            1,
            56,
        ],
    ],
];
